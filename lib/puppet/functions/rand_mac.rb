# Function generates random MAC address
#
# '0e' prefix to ensure generated mac will be in safe to use range
#
Puppet::Functions.create_function(:rand_mac) do
  dispatch :rand_mac do
    return_type 'String'
  end

  def rand_mac
    '0e:' << (1..5).collect { format('%02x', (rand 255)) }.join(':')
  end
end
