# Init Class
#
class kvm {

    $depencies = ['wget']
    $packages = [ 'qemu-kvm', 'libvirt-clients', 'libvirt-daemon-system',
                  'cloud-image-utils', 'libguestfs-tools' ]
    $services = ['libvirtd']

    $puppet = lookup('puppet', Hash)

    $kvm = lookup('kvm', Hash)
    $storage = $kvm['storage']
    $image_storage = "${storage}/images"
    $xml_storage = "${storage}/xml"
    $local_storage = "${storage}/local"
    $images = $kvm['images']
    $instances = $kvm['instances']
    $interface = $kvm['interface']

}
