# Download cloud OS image
# 
# @images        - hash with images parameters
# @store         - full path to folder to store images
# @depends       - requirements to run this function
#
define kvm::utils::image (
    Hash $images,
    String $store,
    Optional[Any] $depends = undef

  ) {


  # ensure folder for images download exist
  if !defined(File[$store]) {
    file {  $store:
      ensure => 'directory',
      owner  => 'libvirt-qemu',
      group  => 'libvirt-qemu',
    }
  }

  # upload cloud OS images
  $images.each |$image, $value| {

    $link = $value['link']
    # extract image extension from download url
    $extention = regsubst($link, '^(.*[\\\.])', '')

    exec { "download_${image}":
      command  => "/usr/bin/wget -O ${store}/${image}.${extention} ${link}",
      provider => 'shell',
      cwd      => $store,
      creates  => "${store}/${image}.${extention}",
      timeout  => 1800,
      require  => [ File[$store],
                    $depends  ]
    }
  }

}
