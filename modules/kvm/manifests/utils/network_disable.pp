# Disable libvirt network
# 
# @network       - network to disable
# @depends       - requirements to run this function
#
define kvm::utils::network_disable (
    String $network,
    Any $depends

  ) {

  # delete libvirt default network
  exec { "libvirt_disable_network_${network}":
    command  => "/usr/bin/virsh net-destroy ${network}
                /usr/bin/virsh net-undefine ${network}",
    provider => 'shell',
    onlyif   => "/usr/bin/virsh net-list --all | /usr/bin/grep ${network}",
    require  => $depends
  }

}

