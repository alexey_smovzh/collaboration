### 0. Description
This module install KVM with cloud-init utils and provides
some utilities for instance creation and launch.

In this module are assumed that all instances will configure network
interfaces via dhcp. So there are no code for providing static network
configuration and also this KVM setup require some DHCP server installed
and configured to advertise network configuration to instances.

*note: check instance.pp and network.erb in commit [ca6f356](https://bitbucket.org/alexey_smovzh/openstack_controller/commits/ca6f3564533aedb0805d4c4bda0b34de469147b1) how to add*
*static network configuration if it will be necessary some time in feature*


### 1. Tested environments
Debian 10 Buster
For Ubuntu some packages will have different names


### 2. Usage
Provide module configuration information in appropriate Hiera hierarchy file.

```
    # Flavors
    # you can provide instance resource configuration in each instance description individually
    # or you can create typical instance configurations and refer to it from instance description
    tiny: &tiny
        memory: 512
        vcpus: 1
        disk: 2G
        os: debian-10-amd64

    small: &small    
        memory: 1024
        vcpus: 1
        disk: 4G
        os: debian-10-amd64


    # Virtualization
    kvm:
        # folder full path to instance data storage
        # under this root folder will be created
        # ${storage}/images     - for OS cloud images download
        # ${storage}/xml        - XML files with instances configuration
        # ${storage}/local      - local storage folder for mount dbrd device 

        storage: /instances

        # physical interface to bind virtual networks
        interface: br-ex
        
        # list of images to download
        # note: this should be cloud images of course 
        #       with cloud-init software installed
        images: 
            # image name
            # note: under this name image will be stored in 'image_store' folder
            #       and this name you should use in instance configuration 'os' parameter
            #       to choose instance operating system
            cirros-0.5.1-x86_64:            
               link: http://download.cirros-cloud.net/0.5.1/cirros-0.5.1-x86_64-disk.img
            debian-10-amd64:
                link: https://cloud.debian.org/images/cloud/buster/20200928-407/debian-10-generic-amd64-20200928-407.qcow2

        # Instances
        # note: instance name parameter used for naming instance in KVM host
        #       also this name are assigned as instance hostname
        #       and in some cases it also used as Puppet role
        #
        # note: hostname can contain letters a-z, digits 0-9 and hyphen '-'
        #       all other symbols are forbitten
        instances:
            # instance name and instance resource configuration
            puppet-server: *small    
            dns-server: *tiny
            jenkins: *small
            mariadb-cicd: *small

```


Install KVM

```
    include kvm::install
```


Update firewall rules on existing instance

*note: there no possibility to update firewall rules when instance running*
*before updating we need to stop instance first*

```
    # stop instance
    sudo virsh destroy <instance>
    
    # list of all available filters
    sudo virsh nwfilter-list

    # edit filter 
    sudo virsh nwfilter-edit <instance>-traffic

    # start instance
    sudo virsh start <instance>
```

### 3. Known backgrounds and issues
can't update firewall rules on running instance


### 4. Used documentation

https://wiki.debian.org/KVM#Installation

https://fabianlee.org/2020/02/23/kvm-testing-cloud-init-locally-using-kvm-for-an-ubuntu-cloud-image/

https://libvirt.org/formatnwfilter.html

https://wiki.libvirt.org/page/Networking

libvirt firewall: https://libvirt.org/formatnwfilter.html

