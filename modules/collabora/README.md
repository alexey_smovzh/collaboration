### 0. Description
This module install Collabora Online Office Suite.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Collabora Office Suite Service listen on localhost interface.
All connections to Collabora goes throught Apache2 configured 
as reverse proxy and as TLS terminator.
Provide configuration for CfSSL in Heira yaml file.

```
    ca:
    home: '/opt/cfssl'
    certificates:
        apache2:
        profile: 'service'
        path: '/opt/cfssl/apache2'
        owner: 'www-data'
        group: 'www-data'
```

Install Collabora Online

```
    include cfssl::manager
    include apache2::install
    include collabora::install


    Class['cfssl::manager']
    -> Class['apache2::install']
    -> Class['collabora::install']
```


### 3. Known backgrounds and issues
none


### 4. Used documentation
https://www.collaboraoffice.com/code/linux-packages/

https://www.collaboraoffice.com/code/apache-reverse-proxy/
