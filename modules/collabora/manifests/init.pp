# Init Class
#
class collabora {


  # Hiera values
  $ca         = lookup('ca.certificates.apache2', Hash)
  $ca_path    = $ca['path']


  # static values
  $key        = '0C54D189F4BA284D'
  $repository = '/etc/apt/sources.list.d/collabora.list'
  $value      = 'deb [arch=amd64] https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian10 ./'


  $user       = 'lool'
  $group      = $user


  $depencies = [  'fonts-dejavu',
                  'fonts-liberation',
                  'ttf-mscorefonts-installer',
                  'fonts-crosextra-carlito',
                  'fonts-takao-gothic',
                  'fonts-opensymbol'  ]

  $packages  = [  'loolwsd', 'code-brand' ]

  $services  = [  'loolwsd'  ]


}
