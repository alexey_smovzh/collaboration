# Install Collabora Online
#
class collabora::install inherits collabora {


  # enable contrib repository to install MS fonts
  exec { 'enable_contrib_repository':
    command  => '/usr/bin/sed -i "s/$/ contrib/" /etc/apt/sources.list',
    provider => 'shell',
    before   => Apt::Utils::Apt_repository['collabora'],
    unless   => '/usr/bin/grep -r contrib /etc/apt/sources.list'
  }

  # install Collabora apt repository
  apt::utils::apt_repository { 'collabora':
    key        => $collabora::key,
    file       => $collabora::repository,
    repository => $collabora::value
  }

  # check if it not already defined and install depencies
  $collabora::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure  => installed,
        require => Apt::Utils::Apt_repository['collabora']
      }
    }
  }

  # install packages
  package { $collabora::packages:
    ensure  => installed,
    require => Package[$collabora::depencies]
  }

  # Configuration file
  file { '/etc/loolwsd/loolwsd.xml':
    ensure  => present,
    owner   => $collabora::user,
    group   => $collabora::group,
    content => epp('collabora/loolwsd.xml.epp'),
    notify  => Service[$collabora::services],
    require => Package[$collabora::packages]
  }

  # ensure service is running and enabled
  service { $collabora::services:
    ensure  => running,
    enable  => true,
    require => File['/etc/loolwsd/loolwsd.xml']
  }

  # enable apache2 modules
  apache2::utils::enable_module { 'enable_collabora_apache_modules':
    modules => ['ssl', 'proxy', 'proxy_wstunnel', 'proxy_http'],
    depends => File['/etc/loolwsd/loolwsd.xml']
  }

  # configure apache site
  apache2::utils::site { 'create_collabora_apache_site':
    site    => $::fqdn,
    config  => epp('collabora/site.epp'),
    depends => Apache2::Utils::Enable_module['enable_collabora_apache_modules']
  }

  # after all packages installed and all configuration are made
  # restart apache2 to be shure that all parametes are applyed correctly
  apache2::utils::restart { 'final_apache2_collabora_restart': }


}
