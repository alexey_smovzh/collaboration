### 0. Description
This module add or disable(not remove) administrator accounts on managed systems.
Account information it takes from administrators section from Hiera.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS, 18.04LTS, Debian 10.


### 2. Usage
Add admin information to Hiera (for example global.yaml for whole infrastructure):

```
# password generated with command: mkpasswd  -m sha-512 -S saltsalt -s
alex:
  enabled: true
  fullname: Alex
  email: alexeysmovzh@gmail.com
  password: <password>
  # public ssh key
  # just key, without 'ssh-rsa' prefix and email address at the end 
  ssh_key: <ssh-public-key>  
```

To disable user account - set value 'false' to 'enabled' account option.
Do not delete user information from yaml file, just change this key value.

```
# password generated with command: mkpasswd  -m sha-512 -S saltsalt -s
alex:
  enabled: false
  fullname: Alex
  email: alexeysmovzh@gmail.com
  password: <password>
  ssh_key: <ssh-public-key>  
```


### 3. Known backgrounds and issues
Not found any yet


### 4. Used documentation
