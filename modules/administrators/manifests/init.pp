# Init Class
#
class administrators {


  # manage Linux system accounts
  administrators::utils::passwd { 'manage_administrator_accounts':
    admins => lookup('administrators', Hash)
  }


}
