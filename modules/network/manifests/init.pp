# Init Class
# Lookups network configuration from Hiera.
# Then applys different manifests for different Host
# netwrokd configuration aspects.
#
class network {


  $network = lookup('network', Hash)

  # make further actions only if we have 
  # network configuration for given host
  if $network[$hostname] {

    $interfaces = $network[$hostname]['interfaces']

    # on Debian previously install netplan  
    if $::operatingsystem == 'debian' {
      include network::netplan
    }

    include network::interfaces
    include network::hosts
    include network::ip_v6
    include network::ip_forwarding

  }

}
