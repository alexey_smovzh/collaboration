# Manage IPv6 Host settings
#
class network::ip_v6 inherits network {


  if $network::network[$::hostname]['ip_v6'] == true {

    # Enable IPv6
    sysctl::utils::update { 'enable_ip_v6':
      options => { 'net.ipv6.conf.all.disable_ipv6' => '0' }
    }

  } else {

    # Disable IPv6
    sysctl::utils::update { 'disable_ip_v6':
      options => { 'net.ipv6.conf.all.disable_ipv6' => '1' }
    }

  }

}
