# Manage IP Forwarding Host configuration
#
class network::ip_forwarding inherits network {


  if $network::network[$::hostname]['ip_forwarding'] == true {

    # Enable IP forwarding
    sysctl::utils::update { 'enable_ip_forwarding':
      options => { 'net.ipv4.ip_forward' => '1' }
    }

  } else {

    # Disable IP forwarding
    sysctl::utils::update { 'disable_ip_forwarding':
      options => { 'net.ipv4.ip_forward' => '0' }
    }

  }

}
