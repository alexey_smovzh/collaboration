# Install Netplan.io network tools
# Install Netplan.io. Ensurea service is enabled and running.
# Purge ifupdown scripts
#
class network::netplan inherits network {

  package { 'netplan.io':
    ensure => installed
  }

  # disable networking.service
  service { 'networking':
    ensure  => stopped,
    enable  => false,
    require => Package['netplan.io']
  }

  # delete ifupdown
  package { 'ifupdown':
    ensure  => purged,
    require => Service['networking']
  }

}
