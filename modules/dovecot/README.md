### 0. Description
This module install Dovecot mail server.

Configuration properties:

- Dovecot installed as IMAP server only. Without POP3 support.
- Dovecot receive messages from MTA over LMTP
- Has global script Sieve support and place message with "X-Spam" flag to 'Spam' folder
- Has ImapSieve support to send message to teach Rspamd when user 
move message to 'Spam' folder or from it
- Has SieveManage configured to execute User defined Sieve filter scripts


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
In this configuration all connections (IMAP and LMTP) are operates
only over SSL. So this module also require CFSSL module to be installed
to manage SSL certificates.

Provide parameters in Hiera yaml file

```
  ca:
    home: '/opt/cfssl'
    certificates:
      dovecot:
        profile: 'service'
        path: '/opt/cfssl/dovecot'
        owner: 'dovecot'
        group: 'dovecot'


  dovecot:
    version: 'ce-2.3-latest'
```  

And install 'rspamc' tool, CFSSL manager and Dovecot

```
  include rspamd::client
  include dovecot::install
  include cfssl::manager
  include dovecot::configure


  # CFSSL need that user dovecot already exists before installation
  # so firstly install Dovecot packages wich are create
  # user 'dovecot' as well. Then install CFSSL. And at last 
  # create Dovecot configureation and run service
  Class['rspamd::client']
  -> Class['dovecot::install']
  -> Class['cfssl::manager']
  -> Class['dovecot::configure']
```

Useful commands
```
  # check user quota (result in kylobytes)
  doveadm quota get -u jane

  # check all quotas       
  doveadm quota get -A

  # find user mailbox guid
  sudo doveadm search -u olya mailbox inbox

  # checkout message IMAP attributes, headers, etc.
  sudo doveadm fetch -u olya "flags uid" mailbox-guid <mailbox-guid>

  # test Sieve filter script
  sieve-test -u olya dovecot.sieve /var/mail/west/olya/Maildir/cur/*

  # another way to test Sieve filter script
  sieve-filter -v -C -e -W -u jesper dovecot.sieve 'INBOX'
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
https://repo.dovecot.org/

https://rspamd.com/doc/tutorials/feedback_from_users_with_IMAPSieve.html
