# Init class
#
class dovecot {


  # Hiera values

  # organization division unit
  $unit = lookup('unit', String, 'first', '')


  # level: profile/collaboration
  $ca            = lookup('ca.certificates.dovecot', Hash)
  $ca_path       = $ca['path']

  $ldap          = lookup('products.ldap', Hash)
  $ldap_host     = $ldap['host']
  $ldap_password = $ldap['unit_admin_password']
  $ldap_unit_dn  = "ou=${unit},dc=${::domain}"

  $spam          = lookup('products.spam.host', String)

  $dovecot       = lookup('dovecot', Hash)
  $version       = $dovecot['version']


  # static values
  $repository = '/etc/apt/sources.list.d/dovecot.list'
  $key        = 'https://repo.dovecot.org/DOVECOT-REPO-GPG'
  $value      = "deb [arch=amd64] https://repo.dovecot.org/${version}/debian/${::lsbdistcodename} ${::lsbdistcodename} main"


  $user       = 'dovecot'
  $group      = $user


  # folder to store mailboxes
  $mailboxes  = "/var/mail/${::domain}"
  $sieve      = '/var/mail/sieve'

  $filters    = ['global', 'spam', 'notspam']


  $packages   = [ 'dovecot-core',
                  'dovecot-imapd',
                  'dovecot-lmtpd',
                  'dovecot-ldap',
                  'dovecot-sieve',
                  'dovecot-managesieved'  ]
  $services   = [ 'dovecot' ]


}
