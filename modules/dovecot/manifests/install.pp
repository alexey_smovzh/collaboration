# Install Dovecot packages
#
class dovecot::install inherits dovecot {


  # install dovecot apt repository
  apt::utils::apt_repository { 'dovecot':
    key        => $dovecot::key,
    file       => $dovecot::repository,
    repository => $dovecot::value,
  }

  # install packages
  package { $dovecot::packages:
    ensure  => installed,
    require => Apt::Utils::Apt_repository['dovecot']
  }


}
