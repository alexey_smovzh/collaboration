# Configure Dovecot
#
class dovecot::configure inherits dovecot {


  # create mailboxes folder
  file { [  $dovecot::mailboxes,
            $dovecot::sieve ]:
    ensure => 'directory',
    owner  => $dovecot::user,
    group  => $dovecot::group,
    mode   => '0775'
  }

  # dovecot settings in one file
  file { '/etc/dovecot/dovecot.conf':
    ensure  => present,
    content => epp('dovecot/dovecot.conf.epp'),
    notify  => Service[$dovecot::services],
    require => File[  $dovecot::mailboxes,
                      $dovecot::sieve ]
  }

  # LDAP connection configuration
  # since it used in two places in passdb and userdb 
  # its better to put it in separate file and refer from main config
  file { '/etc/dovecot/dovecot-ldap.conf':
    ensure  => present,
    content => epp('dovecot/ldap.conf.epp'),
    notify  => Service[$dovecot::services],
    require => File['/etc/dovecot/dovecot.conf']
  }

  # create Sieve filter files
  $dovecot::filters.each |$filter| {

    # create config
    file { "${dovecot::sieve}/${filter}.sieve":
      ensure  => present,
      content => epp("dovecot/${filter}.sieve.epp"),
      notify  => Exec["compile_sieve_filter_${filter}"],
      require => File['/etc/dovecot/dovecot-ldap.conf']
    }

    # compile filter
    exec { "compile_sieve_filter_${filter}":
      command     => "/usr/bin/sievec -c /etc/dovecot/dovecot.conf ${dovecot::sieve}/${filter}.sieve",
      provider    => 'shell',
      notify      => Service[$dovecot::services],
      refreshonly => true
    }

  }

  # run service
  service { $dovecot::services:
    ensure  => running,
    enable  => true,
    require => File[$dovecot::sieve]
  }


}
