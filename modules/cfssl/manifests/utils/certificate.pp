# Create Certificate Specification for CFSSL certmgr
# 
# @certificate  - Certificate name. Usualy this is just service name 
#                 for which certificate are created (apache2 for example)
# @profile      - Name of pki profile from 'ca-config.json'
# @path         - Full path to folder where certificates should stored
# @owner        - System user owner of certificate ('www-data' for Apache2 for example)
# @group        - System group owner of certificate ('www-data' for Apache2 for example)
#
# @home         - full path to cfssl installation
# @server       - CFSSL server FQDN
# @port         - CFSSL server network port
# @token        - SHA256-HMAC token to authentificate on CFSSL server
#
# @country_code - Country code (e.q. 'UA')
# @country      - Country name
# @city         - City name
# @organization - Organization name
# @unit         - Organization unit (e.q. Root CA Name)
#
# @depends      - requirements to run this function
#
define cfssl::utils::certificate (

    String $certificate,
    String $profile,
    String $path,
    String $owner,
    String $group,
    String $home,
    String $server,
    String $port,
    String $token,
    String $country_code,
    String $country,
    String $city,
    String $organization,
    String $unit,
    Optional[Any] $depends = undef

  ) {


  # create folder for certificate
  file { $path:
    ensure  => 'directory',
    owner   => $owner,
    group   => $group,
    recurse => true,
    require => $depends
  }

  # create manager config
  file {  "${path}/certmgr.yaml":
    ensure  => present,
    owner   => $owner,
    group   => $group,
    content => template('cfssl/certmgr.yaml.erb'),
    notify  => Service["cfssl-mgr-${certificate}"],
    require => File[$path]
  }

  # create certificate specification 
  file {  "${path}/pki-spec.json":
    ensure  => present,
    owner   => $owner,
    group   => $group,
    content => template('cfssl/pki-spec.json.erb'),
    notify  => Exec["generate_${certificate}"],
    require => File[$path],
  }

  # generate certificates manually to ensure that they exist 
  # to create symlink to root public certificate on next step
  exec { "generate_${certificate}":
    command     => "${home}/certmgr ensure -f ${path}/certmgr.yaml -d ${path}/pki-spec.json",
    cwd         => $home,
    user        => $owner,
    group       => $group,
    refreshonly => true,
    require     => [  File["${path}/certmgr.yaml"],
                      File["${path}/pki-spec.json"] ]
  }

  # create symlink to certificate
  file { "/etc/ssl/certs/${::domain}.pem":
    ensure  => 'link',
    target  => "${path}/ca-root.pem",
    require => Exec["generate_${certificate}"]
  }

  # create service file
  file { "/lib/systemd/system/cfssl-mgr-${certificate}.service":
    ensure  => present,
    content => epp('cfssl/service.epp', { unit => 'manager', home => $home, path => $path }),
    notify  => Service["cfssl-mgr-${certificate}"],
    require => File["/etc/ssl/certs/${::domain}.pem"]
  }

  # ensure service is running and enabled
  service { "cfssl-mgr-${certificate}":
    ensure  => running,
    enable  => true,
    require => File["/lib/systemd/system/cfssl-mgr-${certificate}.service"]
  }

}
