# Install CFSSL Manager
#
# Tool for automatic generation and renewal of services certificates
#
class cfssl::manager inherits cfssl::install {


  # extract downloaded file name from url
  $file_long = regsubst($cfssl::certmgr, '^(.*[\\\/])', '')
  # remove version and architecture information from file name
  $file_short =regsubst($file_long, '\-.*', '')

  # download archive, unpack, rename, make executable, delete archive
  exec { 'download_certmgr':
    command  => "/usr/bin/wget -q ${cfssl::certmgr} \
              && /usr/bin/tar xf ${file_long} \
              && /usr/bin/rm -f ${file_long} \
              && /usr/bin/mv certmgr* ${file_short}",
    provider => 'shell',
    cwd      => $cfssl::home,
    user     => $cfssl::user,
    group    => $cfssl::group,
    creates  => "${cfssl::home}/${file_short}",
    require  => User[$cfssl::user]
  }

  # setup certificates specifications and create certmanager service
  $cfssl::certificates.each |$certificate, $values| {

    cfssl::utils::certificate{ "setup_certificate_${certificate}":
      certificate  => $certificate,
      profile      => $values['profile'],
      path         => $values['path'],
      owner        => $values['owner'],
      group        => $values['group'],
      home         => $cfssl::home,
      server       => $cfssl::server,
      port         => $cfssl::port,
      token        => $cfssl::token,
      country_code => $cfssl::ca_country_code,
      country      => $cfssl::ca_country,
      city         => $cfssl::ca_city,
      organization => $cfssl::ca_organization,
      unit         => $cfssl::ca_unit,
      depends      => Exec['download_certmgr']
    }

  }

}
