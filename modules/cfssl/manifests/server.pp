# Install CFSSL binaries and run cfssl as network server
#
# Download CFSSL binaries, create root certificate configuration,
# create Root Certification Authority configuration, 
# generate root certificate and root certificate bundle,
# define CFSSL as service and ensures it enabled and running
#
class cfssl::server inherits cfssl::install {


  # download binaries
  $cfssl::binaries.each |$link| {

    cfssl::utils::download { "download_${link}":
      link    => $link,
      bin     => $cfssl::home,
      user    => $cfssl::user,
      group   => $cfssl::group,
      earlier => File["${cfssl::home}/root"]
    }

  }

  # create folder for root certificate
  file { "${cfssl::home}/root":
    ensure  => 'directory',
    owner   => $cfssl::user,
    group   => $cfssl::group,
    recurse => true,
    require => User[$cfssl::user]
  }

  # create root certificate configuration 
  file {  "${cfssl::home}/root/ca-csr.json":
    ensure  => present,
    owner   => $cfssl::user,
    group   => $cfssl::group,
    content => epp('cfssl/ca-csr.json.epp'),
    notify  => Service['cfssl-root'],
    require => File["${cfssl::home}/root"]
  }

  file {  "${cfssl::home}/root/ca-config.json":
    ensure  => present,
    owner   => $cfssl::user,
    group   => $cfssl::group,
    content => epp('cfssl/ca-config.json.epp'),
    notify  => Service['cfssl-root'],
    require => File["${cfssl::home}/root"]
  }

  # generate root certificate
  exec { 'generate_root_ca':
    command  => "${cfssl::home}/cfssl gencert -initca ca-csr.json | \
                 ${cfssl::home}/cfssljson -bare ca -",
    provider => 'shell',
    cwd      => "${cfssl::home}/root",
    user     => $cfssl::user,
    group    => $cfssl::group,
    creates  => "${cfssl::home}/root/ca.pem",
    notify   => Service['cfssl-root'],
    require  => File[ "${cfssl::home}/root/ca-csr.json",
                      "${cfssl::home}/root/ca-config.json"  ]
  }

  # create root ca bundle to import on clients
  exec { 'create_root_ca_bundle':
    command  => "${cfssl::home}/mkbundle -f ${cfssl::home}/root/${cfssl::ca_unit}.crt ${cfssl::home}/root",
    provider => 'shell',
    cwd      => "${cfssl::home}/root",
    user     => $cfssl::user,
    group    => $cfssl::group,
    creates  => "${cfssl::home}/root/${cfssl::ca_unit}.crt",
    require  => Exec['generate_root_ca']
  }

  # create service file
  file { '/lib/systemd/system/cfssl-root.service':
    ensure  => present,
    content => epp('cfssl/service.epp', { unit => 'server' }),
    notify  => Service['cfssl-root'],
    require => Exec['generate_root_ca']
  }

  # ensure service is running and enabled
  service { 'cfssl-root':
    ensure  => running,
    enable  => true,
    require => File['/lib/systemd/system/cfssl-root.service']
  }

}
