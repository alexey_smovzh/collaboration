# Install and configure OpenProject
#
class openproject::install inherits openproject {


  # install openproject apt repository
  apt::utils::apt_repository { 'openproject':
    key        => $openproject::key,
    file       => $openproject::repository,
    repository => $openproject::value
  }

  # install packages
  package { $openproject::packages:
    ensure  => installed,
    require => [Apt::Utils::Apt_repository['openproject'],
                Package[$openproject::depencies]]
  }

  # add openproject bin folder to PATH
  file { '/etc/profile.d/openproject.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${openproject::home}/bin\"",
    require => Package[$openproject::packages]
  }

  # wait 10 minutes to postgresql brings alive
  # this should be enough in most cases
  # if it not come alive within 10 minutes manifest throw error
  # and user can restart module later or maybe found and fix error in configuration
  exec { 'wait_postgresql':
    command => "/usr/bin/bash -c 'until /usr/bin/pg_isready -h ${openproject::db_host}; do /usr/bin/sleep 5; done'",
    timeout => 600,
    require => File['/etc/profile.d/openproject.sh']
  }

  # create openproject database
  postgresql::utils::database { 'create_openproject_database':
    db            => $openproject::db,
    administrator => $openproject::db_administrator,
    password      => $openproject::db_password,
    host          => $openproject::db_host,
    depends       => Exec['wait_postgresql']
  }

  # Create OpenProject database user
  postgresql::utils::user { 'create_openproject_database_user':
    db            => $openproject::db,
    administrator => $openproject::db_administrator,
    password      => $openproject::db_password,
    host          => $openproject::db_host,
    user          => $openproject::user,
    user_password => $openproject::password,
    depends       => Postgresql::Utils::Database['create_openproject_database']
  }

  # create answer file
  file { '/etc/openproject/installer.dat':
    ensure  => present,
    owner   => $openproject::user,
    group   => $openproject::group,
    content => epp('openproject/installer.dat.epp'),
    notify  => Exec['initial_configuration'],
    require => Postgresql::Utils::User['create_openproject_database_user']
  }

  # populate database and create initial configuration
  exec { 'initial_configuration':
    command     => '/usr/bin/openproject configure',
    provider    => 'shell',
    cwd         => $openproject::home,
    user        => 'root',
    group       => 'root',
    timeout     => 600,
    refreshonly => true,
    require     => File['/etc/openproject/installer.dat']
  }

  # create configuration file
  file { "${openproject::home}/config/configuration.yml":
    ensure  => present,
    owner   => $openproject::user,
    group   => $openproject::group,
    content => epp('openproject/configuration.yml.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_openproject_restart'],
    require => Exec['initial_configuration']
  }

  # Update settings
  postgresql::utils::runsql { 'update_openproject_settings':
    db            => $openproject::db,
    host          => $openproject::db_host,
    administrator => $openproject::user,
    password      => $openproject::password,
    query         => epp('openproject/settings.psql.epp'),
    check         => 'SELECT value FROM settings WHERE name = \'settings_updated\'',
    depends       => File["${openproject::home}/config/configuration.yml"]
  }

  # ensure openproject services running and enabled
  service { $openproject::services:
    ensure   => running,
    provider => 'systemd',
    enable   => true,
    require  => Postgresql::Utils::Runsql['update_openproject_settings']
  }

  # enable apache2 modules
  apache2::utils::enable_module { 'enable_openproject_apache_modules':
    modules => ['ssl', 'headers', 'expires', 'proxy', 'proxy_http'],
    depends => Service[$openproject::services]
  }

  # configure apache site
  apache2::utils::site { 'create_openproject_apache_site':
    site    => $::fqdn,
    config  => epp('openproject/site.epp'),
    depends => Apache2::Utils::Enable_module['enable_openproject_apache_modules']
  }

  # after all packages installed and all configuration are made
  # restart apache2 to be shure that all parametes are applyed correctly
  apache2::utils::restart { 'final_apache2_openproject_restart': }


}
