# Init class
#
class openproject {


  # Hiera values
  $units            = lookup('units', Tuple)

  $ldap             = lookup('products.ldap', Hash)
  $ldap_host        = $ldap['host']
  $ldap_password    = $ldap['unit_viewer_password']

  $database         = lookup('products.database', Hash)
  $db_host          = $database['host']
  $db_administrator = $database['administrator']
  $db_password      = $database['password']


  # Because access to OpenProject database must have only OpenProject itself
  # user name and password credentials are hardcoded
  $db       = 'openproject'
  # todo: encrypt!!
  $password = 'openproject'
  $home     = '/opt/openproject'
  $user     = 'openproject'
  $group    = $user


  $repository = '/etc/apt/sources.list.d/openproject.list'
  $key        = 'https://dl.packager.io/srv/opf/openproject/key'
  $value      = 'deb https://dl.packager.io/srv/deb/opf/openproject/stable/11/debian 10 main'


  $packages   = [ 'openproject' ]
  $services   = [ 'openproject', 'openproject-worker', 'openproject-web',
                  'openproject-worker-1', 'openproject-web-1' ]


}
