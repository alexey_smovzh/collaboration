### 0. Description
This module install OpenProject.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage


### 3. Known backgrounds and issues



### 4. Used documentation

https://github.com/madewhatnow/OpenProjectRaspberryPi/blob/master/index.md

https://www.openproject.org/docs/installation-and-operations/
