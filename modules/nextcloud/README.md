### 0. Description
This module install NextCloud.

Also if enabled it enables integration with Redmine 
and Collabora Online Office Suite. Check [collaboration.yaml](../../hiera/profile/collaboration.yaml)
'products' section for details.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Provide configuration in Hiera yaml file

```
  # NextCloud to improve performance heavily rely on caching
  # For caching purpose in this setup are used MemCached
  # Since MemCached used by NexctCloud on same instance 
  # it listens on localhost interface only
  memcached:
    bind: '127.0.0.1'


  # TLS certificate parameters for Apache2 web server 
  # to secure connections
  ca:
    home: '/opt/cfssl'
    certificates:
      apache2:
        profile: 'service'
        path: '/opt/cfssl/apache2'
        owner: 'www-data'
        group: 'www-data'


  # NextCloud configuration
  nextcloud: 
    # Version to install 
    # before installation check latest stable version 
    # https://nextcloud.com/install/
    version: '22.1.0'
    # Full path where to place NextCloud files
    home: '/var/www/nextcloud'
    # Full path to folder where user files will be stored
    # note: for security reasons it must be outside the 
    #       NextCloud 'home' folder
    storage: '/nextcloud'
    # Version of PHP to install for NextCloud
    php_version: '7.4'
    # Applications list
    # true  - means application will be downloaded if
    #         it not already and enabled
    # false - means application will be disabled
    #         but not removed
    applications:
      mail: true
      calendar: true
      contacts: true
      tasks: true
      activity: false
      weather_status: false
      user_ldap: true
      # external sites
      external: true
      # Collabora Online 
      richdocuments: true      
```


### 3. Known backgrounds and issues
before deploynment in production adjust RAM allocation limits

current NextCloud version 22.1.0
before apply next version checkout workarounds from postinstall.pp
may be it already fixed


https://github.com/nextcloud/mail/issues/5388

https://github.com/nextcloud/mail/issues/4101


  # todo: federation https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/federated_cloud_sharing_configuration.html



### 4. Used documentation
https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html

https://www.linuxbabe.com/cloud-storage/integrate-collabora-online-server-nextcloud-ubuntu
