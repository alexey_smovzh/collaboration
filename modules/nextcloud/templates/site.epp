# All requests to HTTP port redirect to HTTPS
<VirtualHost *:80>
  ServerName <%= $facts[fqdn] %>
  ServerAlias www.<%= $facts[fqdn] %>
  Redirect permanent / https://<%= $facts[fqdn] %>/
</VirtualHost>

# Configure HTTPS Site
<VirtualHost *:443>
  ServerName <%= $facts[fqdn] %>
  ServerAlias www.<%= $facts[fqdn] %>
  SSLEngine on
  SSLCertificateFile "<%= $nextcloud::ca_path %>/ca.pem"
  SSLCertificateKeyFile "<%= $nextcloud::ca_path %>/ca-key.pem"

  <IfModule mod_headers.c>
    Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
  </IfModule>

  DocumentRoot <%= $nextcloud::home %>

  <Directory <%= $nextcloud::home %>>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    SetEnv HOME <%= $nextcloud::home %>
    SetEnv HTTP_HOME <%= $nextcloud::home %>
    
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
  
</VirtualHost>
