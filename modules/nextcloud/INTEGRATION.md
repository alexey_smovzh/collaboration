## Manual steps required to configure integration between NextCloud and OpenProject


### As administrator

1. Login to OpenProject as administrator (admin/admin)

2. Administration -> Authentication -> OAuth applications

      Name: nextcloud_<unit>
      Redirect URI: https://nextcloud.west/index.php/apps/integration_openproject/oauth-redirect
      Scopes: api_v3
      Confedential: true

3. Copy Client ID and Secret

4. Login to NextCloud as administrator

5. Settings -> Administration -> Connected accounts

      OpenProject instance address: openproject.west
      ID & Secret from step #3


### As user

1. Login to OpenProject as ordinary user

    https://openproject.west/login

2. My Account -> Access token -> Generate API token

3. Login to nextcloud as ordinary user

4. Settings -> Connected accounts

      Enable navigation link: true
      OpenProject instance address: openproject.west
      Access token: insert token from step #2


