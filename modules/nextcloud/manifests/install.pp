# Install NextCloud
#
class nextcloud::install inherits nextcloud {


  # check if it not already defined and install depencies
  $nextcloud::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install php apt repository
  apt::utils::apt_repository { 'php':
    key        => $nextcloud::key,
    file       => $nextcloud::repository,
    repository => $nextcloud::value,
  }

  # install packages
  package { $nextcloud::packages:
    ensure  => installed,
    require => [Apt::Utils::Apt_repository['php'],
                Package[$nextcloud::depencies]]
  }

  # create home folder
  file { [  $nextcloud::home,
            $nextcloud::storage ]:
    ensure  => 'directory',
    owner   => $nextcloud::user,
    group   => $nextcloud::group,
    require => Package[$nextcloud::packages]
  }

  # install nextcloud
  exec { 'install_nextcloud':
    command  => "/usr/bin/wget -q -O ${nextcloud::home}/nextcloud.zip ${nextcloud::link} \
              && /usr/bin/unzip -q ${nextcloud::home}/nextcloud.zip \
              && /usr/bin/mv ${nextcloud::home}/nextcloud/* ${nextcloud::home} \
              && /usr/bin/rm -rf ${nextcloud::home}/nextcloud*",
    provider => 'shell',
    cwd      => $nextcloud::home,
    user     => $nextcloud::user,
    group    => $nextcloud::group,
    creates  => "${nextcloud::home}/index.php",
    require  => File[$nextcloud::home]
  }

  # wait 10 minutes to postgresql brings alive
  # this should be enough in most cases
  # if it not come alive within 10 minutes manifest throw error
  # and user can restart module later or maybe found and fix error in configuration
  exec { 'wait_postgresql':
    command => "/usr/bin/bash -c 'until /usr/bin/pg_isready -h ${nextcloud::db_host}; do /usr/bin/sleep 5; done'",
    timeout => 600,
    require => Exec['install_nextcloud']
  }

  # setup nextcloud
  # to list available options: sudo -u www-data php occ help maintenance:install
  exec { 'setup_nextcloud':
    command  => "/usr/bin/php occ maintenance:install \
                                  --data-dir=${nextcloud::storage} \
                                  --database=pgsql \
                                  --database-host=${nextcloud::db_host} \
                                  --database-name=nextcloud \
                                  --database-user=${nextcloud::db_administrator} \
                                  --database-pass=${nextcloud::db_password} \
                                  --admin-user=${nextcloud::administrator} \
                                  --admin-pass=${nextcloud::password}",
    provider => 'shell',
    cwd      => $nextcloud::home,
    user     => $nextcloud::user,
    group    => $nextcloud::group,
    creates  => "${nextcloud::home}/config/config.php",
    require  => Exec['wait_postgresql']
  }

  # create full configuration
  file {  "${nextcloud::home}/config/.config.template":
    ensure  => present,
    owner   => $nextcloud::user,
    group   => $nextcloud::group,
    content => epp('nextcloud/config.php.epp'),
    notify  => Exec['update_config'],
    require => Exec['setup_nextcloud']
  }

  # copy secret hash to .config.ready
  # copy nextcloud version to .config.ready
  # move .config.ready over config.php
  exec { 'update_config':
    command     => "SECRET=$(/usr/bin/grep secret ${nextcloud::home}/config/config.php | /usr/bin/cut -d\\' -f4); \
                    /usr/bin/sed \"s=REPLACEME=\$SECRET=g\" ${nextcloud::home}/config/.config.template \
                    > ${nextcloud::home}/config/.config.ready \
                 && VERSION=$(/usr/bin/grep version ${nextcloud::home}/config/config.php | /usr/bin/cut -d\\' -f4); \
                    /usr/bin/sed -i \"s=REPLACEVER=\$VERSION=g\" ${nextcloud::home}/config/.config.ready \
                 && /usr/bin/mv ${nextcloud::home}/config/.config.ready ${nextcloud::home}/config/config.php",
    provider    => 'shell',
    cwd         => $nextcloud::home,
    user        => $nextcloud::user,
    group       => $nextcloud::group,
    refreshonly => true,
    notify      => Apache2::Utils::Restart['final_apache2_nextcloud_restart'],
    require     => File["${nextcloud::home}/config/.config.template"]
  }

  # import domain certificate
  exec { 'import_domain_certificate':
    command  => "/usr/bin/php occ security:certificates:import /etc/ssl/certs/${::domain}.pem",
    provider => 'shell',
    cwd      => $nextcloud::home,
    user     => $nextcloud::user,
    group    => $nextcloud::group,
    require  => Exec['update_config'],
    unless   => "/usr/bin/php occ security:certificates | /usr/bin/grep -w ${::domain}.pem"
  }

  # Install applications
  nextcloud::utils::application { 'manage_applications':
    applications => $nextcloud::applications,
    user         => $nextcloud::user,
    group        => $nextcloud::group,
    home         => $nextcloud::home,
    depends      => Exec['import_domain_certificate']
  }

  # create .htaccess 
  file {  "${nextcloud::home}/.htaccess":
    ensure  => present,
    owner   => $nextcloud::user,
    group   => $nextcloud::group,
    content => epp('nextcloud/htaccess.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_nextcloud_restart'],
    require => Nextcloud::Utils::Application['manage_applications']
  }

  # update apache2 php.ini configuration
  file {  "/etc/php/${nextcloud::php_version}/apache2/php.ini":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => epp('nextcloud/php.ini.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_nextcloud_restart'],
    require => File["${nextcloud::home}/.htaccess"]
  }

  # enable apache2 modules
  apache2::utils::enable_module { 'enable_nextcloud_apache_modules':
    modules => ['ssl', 'php', 'rewrite', 'headers', 'env', 'dir', 'mime'],
    depends => File["/etc/php/${nextcloud::php_version}/apache2/php.ini"]
  }

  # configure apache site
  apache2::utils::site { 'create_nextcloud_apache_site':
    site    => "${::hostname}.${::domain}",
    config  => epp('nextcloud/site.epp'),
    depends => Apache2::Utils::Enable_module['enable_nextcloud_apache_modules']
  }

  # after all packages installed and all configuration are made
  # restart apache2 to be shure that all parametes are applyed correctly
  apache2::utils::restart { 'final_apache2_nextcloud_restart': }

}
