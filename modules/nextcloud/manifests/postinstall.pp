# Update NextCloud settings
#
class nextcloud::postinstall inherits nextcloud {


  # Copy Redmine icon
  file { 'copy_redmine_icon':
    path    => "${nextcloud::storage}/appdata_${::hostname}/external/icons",
    source  => 'puppet:///modules/nextcloud',
    owner   => $nextcloud::user,
    group   => $nextcloud::group,
    recurse => true
  }

  # create configuration JSON file
  file {  "${nextcloud::home}/config/settings.json":
    ensure  => present,
    owner   => $nextcloud::user,
    group   => $nextcloud::group,
    content => epp('nextcloud/settings.json'),
    notify  => [  Exec['update_settings'],
                  Exec['fix_password']  ],
    require => File['copy_redmine_icon']
  }

  exec { 'update_settings':
    command     => "/usr/bin/php occ config:import ${nextcloud::home}/config/settings.json",
    cwd         => $nextcloud::home,
    user        => $nextcloud::user,
    group       => $nextcloud::group,
    refreshonly => true
  }

  # workaround of this bug
  # https://github.com/nextcloud/server/issues/27550
  # https://help.nextcloud.com/t/user-ldap-cant-set-ldap-password-via-config-import/118544
  exec { 'fix_password':
    command     => "/usr/bin/php occ ldap:set-config s01 ldapAgentPassword 'alex'",
    provider    => 'shell',
    cwd         => $nextcloud::home,
    user        => $nextcloud::user,
    group       => $nextcloud::group,
    require     => Exec['update_settings'],
    refreshonly => true
  }

  # currently mail app can be configured only via database
  postgresql::utils::runsql { 'configure_nextcloud_mail_provisioning':
    db            => 'nextcloud',
    host          => $nextcloud::db_host,
    administrator => $nextcloud::administrator,
    password      => $nextcloud::password,
    query         => epp('nextcloud/settings.psql.epp')
  }


}
