# Init class
#
class nextcloud {

  ### hiera values

  # organization division unit
  $unit = lookup('unit', String, 'first', '')


  # level: profile/collaboration
  $net_allowed      = lookup('net_allowed', String)

  $ca               = lookup('ca.certificates.apache2', Hash)
  $ca_path          = $ca['path']


  $uname            = lookup('products.unit_database.name', String)
  $db_host          = "${uname}-${unit}.${::domain}"
  $database         = lookup('products.database', Hash)
  $db_administrator = $database['administrator']
  $db_password      = $database['password']


  $ldap             = lookup('products.ldap', Hash)
  $ldap_host        = $ldap['host']
  $ldap_password    = $ldap['unit_viewer_password']
  $ldap_unit_dn     = "ou=${unit},dc=${::domain}"


  $smtp             = lookup('products.smtp.host', String)
  # compile unit imap server hostname
  $imap_name        = lookup('products.imap.name', String)
  $imap             = "${imap_name}-${unit}.${::domain}"


  $redmine          = lookup('products.project.host', String)
  $collabora        = lookup('products.office.host', String)


  $administrator    = lookup('products.file.administrator', String)
  $password         = lookup('products.file.password', String)

  # email attachment size limit
  $attachment_size  = lookup('attachment_size', String)

  # level: role/nextcloud
  $nextcloud        = lookup('nextcloud', Hash)
  $version          = $nextcloud['version']
  $home             = $nextcloud['home']                 # folder where scripts are installed
  $storage          = $nextcloud['storage']              # folder for shared files storage
  $php_version      = $nextcloud['php_version']
  $applications     = $nextcloud['applications']


  ### static values

  # nextcloud download link
  $link      = "https://download.nextcloud.com/server/releases/nextcloud-${version}.zip"


  # apache2 user and group to grant access to nextcloud $home
  $user      = 'www-data'
  $group     = $user


  $repository = '/etc/apt/sources.list.d/php.list'
  $key        = 'https://packages.sury.org/php/apt.gpg'
  $value      = "deb [arch=amd64] https://packages.sury.org/php/ ${::lsbdistcodename} main"


  $depencies  = ['unzip', 'zip']
  $packages   = [ "php${php_version}",
                  "libapache2-mod-php${php_version}",
                  'libmagickcore-6.q16-6-extra',
                  "php${php_version}-pgsql",
                  "php${php_version}-ldap",
                  "php${php_version}-gmp",
                  "php${php_version}-zip",
                  "php${php_version}-bz2",
                  "php${php_version}-curl",
                  "php${php_version}-intl",
                  "php${php_version}-json",
                  "php${php_version}-gd",
                  "php${php_version}-imagick",
                  "php${php_version}-memcached",
                  "php${php_version}-xml",
                  "php${php_version}-mbstring",
                  "php${php_version}-bcmath"  ]

}
