# Install and enable NextCloud application
# or disable if application not needed anymore
#
# note: application are never unistalled just disabled
# 
# @applications - applications names and desired status
# @user         - user account name to run commands from
# @group        - group name to run commands from
# @home         - nexcloud home folder
# @depends      - requirements to run this function
#
define nextcloud::utils::application (

    Hash $applications,
    String $user,
    String $group,
    String $home,
    Any $depends

  ) {

  $applications.each |$app, $state| {


    # check if app already installed and if not install it
    if $state == true {

      exec { "install_application_${app}":
        command  => "/usr/bin/php occ app:install ${app}",
        provider => 'shell',
        user     => $user,
        group    => $group,
        cwd      => $home,
        creates  => "${home}/apps/${app}/appinfo/info.xml",
        require  => $depends
      }

    }

    # enable or disable app
    if $state == true {
      $action   = 'enable'
      $enabled  = 'yes'
    } else {
      $action   = 'disable'
      $enabled  = 'no'
    }

    exec { "manage_application_${app}":
      command  => "/usr/bin/php occ app:${action} ${app}",
      provider => 'shell',
      user     => $user,
      group    => $group,
      cwd      => $home,
      unless   => "/usr/bin/php occ config:app:get ${app} enabled | /usr/bin/grep ${enabled}",
      require  => $depends
    }

  }

}

