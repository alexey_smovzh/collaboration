# Install OpenLDAP
#
class openldap::install inherits openldap {


  # OpenLDAP in Debian are configured by wizard during installation
  # since Puppet install it in unattended way
  # we are predefine answers for wizard questions
  #
  # to view all wizard questions/configuration options use:
  #     sudo debconf-show slapd
  #   or
  #     sudo debconf-get-selections | grep slapd
  #     
  exec { 'set_openldap_install_wizard_apt_answers':
    command  => "/usr/bin/echo 'slapd slapd/password2 password ${openldap::password}' | /usr/bin/debconf-set-selections \
              && /usr/bin/echo 'slapd slapd/password1 password ${openldap::password}' | /usr/bin/debconf-set-selections \
              && /usr/bin/echo 'slapd slapd/backend select MDB' | /usr/bin/debconf-set-selections \
              && /usr/bin/echo 'slapd slapd/domain string ${::domain}' | /usr/bin/debconf-set-selections \
              && /usr/bin/echo 'slapd shared/organization string ${openldap::organization}' | /usr/bin/debconf-set-selections",
    provider => 'shell',
    user     => 'root',
    group    => 'root',
  }

  # install packages
  package { $openldap::packages:
    ensure  => installed,
    require => Exec['set_openldap_install_wizard_apt_answers']
  }

  # create slapd config
  file { '/etc/default/slapd':
    ensure  => present,
    content => epp('openldap/slapd.epp'),
    require => Package[$openldap::packages],
    notify  => Service[$openldap::services]
  }

  # ensure service is running and enabled
  service { $openldap::services:
    ensure  => running,
    enable  => true,
    require => Package[$openldap::packages]
  }

}
