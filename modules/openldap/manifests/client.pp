# Configure LDAP client via /etc/ldap/ldap.conf file
#
class openldap::client inherits openldap {


  file { '/etc/ldap/ldap.conf':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => epp('openldap/ldap.conf.epp')
  }


}
