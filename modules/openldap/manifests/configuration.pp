# Configure LDAP
#
class openldap::configuration inherits openldap {


  $openldap::configs.each |$config| {

    # create ldap config
    file { "/etc/ldap/${config}.ldif":
      ensure  => present,
      content => epp("openldap/${config}.ldif.epp"),
      notify  => Exec["apply_config_${config}"]
    }

    # apply configuration
    exec { "apply_config_${config}":
      command     => "/usr/bin/ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/${config}.ldif",
      provider    => 'shell',
      user        => 'root',
      group       => 'root',
      refreshonly => true
    }

  }

}
