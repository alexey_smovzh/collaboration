# Init class
#
class openldap {


  # hiera values
  $units                 = lookup('units', Tuple)

  $ldap                  = lookup('products.ldap', Hash)
  $password              = $ldap['password']
  $organization          = $ldap['organization']
  $admin_password        = $ldap['unit_admin_password_crypted']
  $viewer_password       = $ldap['unit_viewer_password_crypted']

  # system accounts
  $smtp                  = lookup('products.smtp', Hash)


  # static values
  $ldap_domain           = "dc=${::domain}"


  $packages = ['slapd', 'ldap-utils']
  $services = ['slapd']


  # LDAP DIT configuration files  
  $configs  = [ 'nextcloud',
                'basic',
                'constraints',
                'indexes',
                'ssl',
                'unique',
                'access',
                'dynamic' ]


}

