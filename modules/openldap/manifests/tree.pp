# Upload organization structure LDAP tree
#
class openldap::tree inherits openldap {


  # create tree config
  file { '/etc/ldap/tree.ldif':
    ensure  => present,
    content => epp('openldap/tree.ldif.epp'),
    notify  => Exec['apply_tree']
  }

  # apply configuration
  exec { 'apply_tree':
    command     => "/usr/bin/ldapadd -H ldapi:/// -x -D cn=admin,dc=west -w ${openldap::password} -f /etc/ldap/tree.ldif",
    provider    => 'shell',
    user        => 'root',
    group       => 'root',
    refreshonly => true
  }


}
