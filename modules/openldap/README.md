### 0. Description
This module install OpenLDAP directory server.
To define Organization directory tree (look at [tree.ldif](templates/tree.ldif.epp))
To change various options refer to appropriate ldif file under
templates folder [templates](templates)

Also this module configure some LDAP overlay modules:

- constraints to check user input and ensure values in account 
fields are match to regexp pattern (look at [constraints.ldif](templates/constraints.ldif.epp))

- unique to ensure that uid, mail and user name values are unique inside
the Tree (look at [unique.ldif](templates/unique.ldif.epp))

- dynamic to dynamically create lists of user accounts parameters.
Such as cn=All containing email addresses of all users under the Tree (look at [dynamic.ldif](templates/dynamic.ldif.epp))


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Define OpenLDAP parameters in Hiera yaml. 
Probably good move will be to do it on highest domain level.

*note: Automatic translation in Puppet code domain name like 'ldap.examle.com'*
*to 'dc=ldap,dc=example,dc=com' is possible but it will be overcoding*
*the simplest and more robust way is to redeclare the same domain in LDAP fashion*

```
    # OpenLDAP parameters
    openldap:
      password: 'alex'
      domain: 'west'
      ldap_domain: 'dc=west'      # ldap record format e.q. 'dc=example,dc=com'
      organization: 'West'
```

And install it

```
      include openldap::install
      include cfssl::manager
      include openldap::configuration
      include openldap::tree

      # cfssl manager require 'openldap' user and group 
      # so first install openldap than cfssl manager
      # 
      # OpenLDAP SSL configuration require ssl certificates 
      # so install cfssl manager, create certificates and than configure LDAP
      #
      # Finally upload LDAP configuration and Organization Tree 
      #
      Class['openldap::install'] 
      -> Class['cfssl::manager'] 
      -> Class['openldap::configuration'] 
      -> Class['openldap::tree']
```

To configure LDAP client on instance.
For example for using SSL to connect to LDAP server.
Add openldap client installation to service role manifest. 
And it will update /etc/ldap/ldap.conf with correct settings.

```
    include cfssl::manager
    include openldap::client
    include apache2::install
    
    Class['cfssl::manager']
    -> Class['openldap::client']
    -> Class['apache2::install']
```


Useful commands:

```
    # check connection and admin password 
    ldapwhoami -x -D cn=admin,dc=west -W

    # check current access control rules
    ldapsearch -LLLQ -Y EXTERNAL -H ldapi:/// -b cn=config -s one olcAccess

    # check SSL connection
    LDAPTLS_CACERT=/opt/cfssl/openldap/ca.pem ldapwhoami -H ldaps://openldap.west -x -D cn=admin,dc=west -W

    # test OpenLDAP configuration
    sudo slaptest -u

    # view LDAP configuration
    sudo slapcat
```


### 3. Known backgrounds and issues
For now LDAP Tree are hardcoded in [tree.ldif](templates/tree.ldif.epp) file.
If you need different trees for different LDAP installations move Tree configuration
in appripriate Hiera yaml. Or omit it at all and configure Tree by hand after 
OpenLDAP installation.

# todo: access logging https://www.openldap.org/doc/admin25/overlays.html#Access%20Logging
# todo: audit logging https://www.openldap.org/doc/admin25/overlays.html#Audit%20Logging


### 4. Used documentation
https://wiki.debian.org/LDAP/OpenLDAPSetup

https://www.openldap.org/doc/admin25/access-control.html

https://3bmahv3xwn6030jbn72hlx3j-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/Dynamic-Membership-Handling.pdf
