# Remove unneeded depencies and delete downloaded packages
define apt::utils::apt_clean {

  exec { "${title}_remove":
    command => '/usr/bin/apt autoremove -y',
  }

  exec { "${title}_clean":
    command => '/usr/bin/apt autoclean -y',
  }
}
