### 0. Description
This module install Apache Tomcat Web Application Server and provides several
Puppet defined fuctions for site configuration from another modules.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Install

```
  include tomcat::install
```

Install Camunda web application

```
  # become Tomcat user to download app with right owner
  sudo su - tomcat -s /bin/bash
  
  # Web Application directory
  cd webapps

  # Download Camunda and save with short name 'camunda'
  wget -O camunda.war https://downloads.camunda.cloud/release/camunda-bpm/tomcat/7.15/camunda-webapp-tomcat-standalone-7.15.0.war
```

Then access Camunda in Web Browser
``` 
  https://<hostname_or_IP>:8080/camunda
```


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation
https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-9-on-debian-10
