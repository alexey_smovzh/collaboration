# Install Tomcat
# 
# Install Java, install Tomcat
# Ensure service enabled and running
# Disable default site
#
class tomcat::install inherits tomcat {


  # check if it not already defined and install depencies
  $tomcat::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure  => installed
      }
    }
  }

  # create tomcat user and group
  group { $tomcat::group:
    ensure  => present,
    system  => true,
    require => Package[$tomcat::depencies]
  }

  user { $tomcat::user:
    ensure     => 'present',
    home       => $tomcat::home,
    shell      => '/bin/false',
    groups     => $tomcat::group,
    comment    => 'Tomcat Java Web Application Server',
    password   => '*',
    managehome => true,
    require    => Group[$tomcat::group]
  }

  # extract downloaded file name from url
  $file_long = regsubst($tomcat::link, '^(.*[\\\/])', '')
  # remove version and architecture information from file name
  $file_short =regsubst($file_long, '\-.*', '')

  # download archive, unpack, rename, make executable, delete archive
  exec { 'download_tomcat':
    command  => "/usr/bin/wget -q ${tomcat::link} \
              && /usr/bin/tar xf ${file_long} --strip-components=1 \
              && /usr/bin/rm -f ${file_long}",
    provider => 'shell',
    cwd      => $tomcat::home,
    user     => $tomcat::user,
    group    => $tomcat::group,
    creates  => "${tomcat::home}/bin/startup.sh",
    require  => User[$tomcat::user]
  }

  # Tomcat service file
  file { '/lib/systemd/system/tomcat.service':
    ensure  => present,
    content => epp('tomcat/service.epp'),
    notify  => Service[$tomcat::services],
    require => Exec['download_tomcat']
  }

  # ensure service is running and enabled
  service { $tomcat::services:
    ensure  => running,
    enable  => true,
    require => File['/lib/systemd/system/tomcat.service']
  }

  # disable default applications
  ['ROOT', 'docs', 'examples', 'host-manager', 'manager'].each|$app| {

    tomcat::utils::disable_app { "disable_application_${app}":
      application => $app,
      home        => $tomcat::home,
      depends     => Exec['download_tomcat']
    }

  }


}
