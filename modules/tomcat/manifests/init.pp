# Init Class
#
class tomcat {


  # Static values
  $home        = '/opt/tomcat'
  $user        = 'tomcat'
  $group       = $user


  # Ram limits in megabytes
  $initial_ram = '256'
  $max_ram     = '512'


  $link        = 'https://apache.paket.ua/tomcat/tomcat-9/v9.0.52/bin/apache-tomcat-9.0.52.tar.gz'


  $depencies   = ['default-jre', 'wget']
  $services    = ['tomcat']


}
