# Restart Apache2 web server
#
define tomcat::utils::restart (

  ) {


  if !defined(Exec['restart_tomcat']) {

    # restart tomcat
    exec { 'restart_tomcat':
      command     => '/usr/sbin/systemctl service restart tomcat',
      provider    => 'shell',
      refreshonly => true
    }

  }

}
