# Disable Tomcat application
# 
# @application    - name of application to disable
# @home           - full path to Tomcat server HOME directory
# @depends        - requirements to run this function
#
define tomcat::utils::disable_app (

    String $application,
    String $home,
    Optional[Any] $depends = undef

  ) {


  # disable site
  exec { "disable_app_webapps_${application}":
    command  => "/usr/bin/rm -rf ${home}/webapps/${application}",
    provider => 'shell',
    onlyif   => "/usr/bin/ls -d ${home}/webapps/${application}",
    require  => $depends
  }

  exec { "disable_app_catalina_${application}":
    command  => "/usr/bin/rm -rf ${home}/work/Catalina/localhost/${application}",
    provider => 'shell',
    onlyif   => "/usr/bin/ls -d ${home}/work/Catalina/localhost/${application}",
    require  => $depends
  }


}
