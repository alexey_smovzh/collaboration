# Manage NTP service
# Install NTP package. Disable systemd-timesycn and chrony if installed.
# Configure timezone. Create NTP configuration file. Ensures NTP 
# service are enabled and running. All NTP configuration information
# provided in Hiera 'ntp' section
#
class ntp {

  # gather ntp servers list from hiera
  $ntp      = lookup('ntp', Hash)
  $timezone = $ntp['timezone']
  $servers  = $ntp['servers']


  # install ntp
  package { 'ntp':
    ensure => installed,
  }

  # disable timesyncd and chrony
  service { ['systemd-timesyncd', 'chrony']:
      ensure => stopped,
      enable => false,
  }

  # configure timezone
  exec { 'configure_timezone':
    command => "/usr/bin/timedatectl set-timezone ${timezone}",
    user    => 'root',
    group   => 'root',
    unless  => "/usr/bin/timedatectl | /bin/grep ${timezone}",
  }

  # create config
  file { '/etc/ntp.conf':
    ensure  => present,
    content => template('ntp/ntp.conf.erb'),
    require => Package['ntp'],
    notify  => Service['ntp']
  }

  # ensure service is running and enabled
  service { 'ntp':
    ensure  => running,
    enable  => true,
    require => Package['ntp']
  }

}
