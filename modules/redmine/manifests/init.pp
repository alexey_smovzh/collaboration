# Init class
#
class redmine {


  # Hiera values
  $units            = lookup('units', Tuple)


  $ldap             = lookup('products.ldap', Hash)
  $ldap_host        = $ldap['host']
  $ldap_password    = $ldap['unit_viewer_password']


  $db_host          = lookup('products.database_project.host', String)
  $database         = lookup('products.database', Hash)
  $db_administrator = $database['administrator']
  $db_password      = $database['password']


  $redmine          = lookup('redmine', Hash)
  $db               = $redmine['database']
  $home             = $redmine['home']
  $link             = $redmine['link']
  $attachments      = $redmine['attachments']
  $theme            = $redmine['theme']
  $user             = $redmine['user']
  $group            = $user
  $password         = $redmine['password']


  # static values
  $depencies = [  'ruby',
                  'ruby-dev',
                  'build-essential',
                  'libpq-dev',
                  'libapache2-mod-passenger',
                  'imagemagick',
                  'ghostscript' ]


}
