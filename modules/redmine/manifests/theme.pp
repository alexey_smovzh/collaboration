# Install custom Redmine theme
#
class redmine::theme inherits redmine {


  # convert theme name to all downcase letters
  $theme  = downcase($redmine::theme['name'])
  # combine long folder path to one short variable
  $folder = "${redmine::home}/public/themes/${theme}"

  # create theme folder
  file { $folder:
    ensure  => 'directory',
    owner   => $redmine::user,
    group   => $redmine::group,
    recurse => true
  }

  # download and exctract theme
  exec { 'install_theme':
    command  => "/usr/bin/wget -q -O ${folder}/theme.tar.gz ${redmine::theme['link']} \
              && /usr/bin/tar xf ${folder}/theme.tar.gz --strip-components 1 \
              && /usr/bin/rm -f ${folder}/theme.tar.gz",
    provider => 'shell',
    cwd      => $folder,
    user     => $redmine::user,
    group    => $redmine::group,
    creates  => "${folder}/package.json",
    require  => File[$folder]
  }


}
