# Configure LDAP authentification sources
#
# All checks are made in sql request itself in one transaction
# so there are no any need to check it twice in Puppet
#
class redmine::ldap inherits redmine {


  postgresql::utils::runsql { 'configure_ldap_connections':
    db            => $redmine::db,
    host          => $redmine::db_host,
    administrator => $redmine::user,
    password      => $redmine::password,
    query         => epp('redmine/ldap.psql.epp')
  }


}
