# Install Redmine
#
class redmine::install inherits redmine {


  # check if it not already defined and install depencies
  $redmine::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed
      }
    }
  }

  # create redmine user and group
  group { $redmine::group:
    ensure  => present,
    system  => true,
    require => Package[$redmine::depencies]
  }

  user { $redmine::user:
    ensure     => 'present',
    home       => $redmine::home,
    shell      => '/bin/false',
    groups     => [ $redmine::group, 'www-data' ],
    comment    => 'Redmine system user',
    password   => '*',
    managehome => true,
    require    => Group[$redmine::group]
  }

  # create folder for attachments
  # note: Puppet 'file' does not have analogue to 'mkdir -p' option
  exec { 'create_attachments_folder':
    command => "/usr/bin/mkdir -p ${redmine::attachments} \
             && /usr/bin/chown -R ${redmine::user}:${redmine::group} ${redmine::attachments}",
    user    => 'root',
    group   => 'root',
    creates => $redmine::attachments,
    require => User[$redmine::user]
  }

  # extract file name from link url
  # e.q. 'redmine-4.2.1.tar.gz'
  $archive = regsubst($redmine::link, '^(.*[\\\/])', '')
  # download and extract
  exec { 'install_redmine':
    command  => "/usr/bin/wget -q ${redmine::link} \
              && /usr/bin/tar xf ${archive} --strip-components=1 \
              && /usr/bin/rm -f ${archive}",
    provider => 'shell',
    cwd      => $redmine::home,
    user     => $redmine::user,
    group    => $redmine::group,
    creates  => "${redmine::home}/Rakefile",
    require  => Exec['create_attachments_folder']
  }

  # add redmine bin folder to PATH
  file { '/etc/profile.d/redmine.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${redmine::home}/bin\"",
    require => Exec['install_redmine']
  }

  # wait 10 minutes to postgresql brings alive
  # this should be enough in most cases
  # if it not come alive within 10 minutes manifest throw error
  # and user can restart module later or maybe found and fix error in configuration
  exec { 'wait_postgresql':
    command => "/usr/bin/bash -c 'until /usr/bin/pg_isready -h ${redmine::db_host}; do /usr/bin/sleep 5; done'",
    timeout => 600,
    require => File['/etc/profile.d/redmine.sh']
  }

  # create redmine database
  postgresql::utils::database { 'create_redmine_database':
    db            => $redmine::db,
    administrator => $redmine::db_administrator,
    password      => $redmine::db_password,
    host          => $redmine::db_host,
    depends       => Exec['wait_postgresql']
  }

  # Create redmine database user
  postgresql::utils::user { 'create_redmine_database_user':
    db            => $redmine::db,
    administrator => $redmine::db_administrator,
    password      => $redmine::db_password,
    host          => $redmine::db_host,
    user          => $redmine::user,
    user_password => $redmine::password,
    depends       => Postgresql::Utils::Database['create_redmine_database']
  }

  # create database connection configuration file
  file { "${redmine::home}/config/database.yml":
    ensure  => present,
    owner   => $redmine::user,
    group   => $redmine::group,
    content => epp('redmine/database.yml.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_redmine_restart'],
    require => Postgresql::Utils::User['create_redmine_database_user']
  }

  # create configuration file
  file { "${redmine::home}/config/configuration.yml":
    ensure  => present,
    owner   => $redmine::user,
    group   => $redmine::group,
    content => epp('redmine/configuration.yml.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_redmine_restart'],
    require => File["${redmine::home}/config/database.yml"]
  }

  # redmine settings
  file { "${redmine::home}/config/settings.yml":
    ensure  => present,
    owner   => $redmine::user,
    group   => $redmine::group,
    content => epp('redmine/settings.yml.epp'),
    notify  => Apache2::Utils::Restart['final_apache2_redmine_restart'],
    require => File["${redmine::home}/config/configuration.yml"]
  }

  # install Bundler
  exec { 'install_bundler':
    command  => '/usr/bin/gem install bundler',
    provider => 'shell',
    user     => 'root',
    group    => 'root',
    creates  => '/usr/local/bin/bundle',
    require  => File["${redmine::home}/config/settings.yml"]
  }

  # install gem depencies
  exec { 'install_gem_depencies':
    command  => "${redmine::home}/bin/bundle config set --local without 'development test' \
              && ${redmine::home}/bin/bundle config set --local path vendor/bundle \
              && ${redmine::home}/bin/bundle install",
    provider => 'shell',
    user     => $redmine::user,
    group    => $redmine::group,
    cwd      => $redmine::home,
    creates  => "${redmine::home}/vendor/bundle",
    require  => Exec['install_bundler']
  }

  # generate secret token
  exec { 'generate_token':
    command  => "${redmine::home}/bin/bundle exec rake generate_secret_token",
    provider => 'shell',
    user     => $redmine::user,
    group    => $redmine::group,
    cwd      => $redmine::home,
    creates  => "${redmine::home}/config/initializers/secret_token.rb",
    require  => Exec['install_gem_depencies']
  }

  # populate database
  exec { 'populate_database':
    command     => "${redmine::home}/bin/bundle exec rake db:migrate \
                 && ${redmine::home}/bin/bundle exec rake redmine:load_default_data",
    provider    => 'shell',
    user        => $redmine::user,
    group       => $redmine::group,
    environment => [  'RAILS_ENV=production', 'REDMINE_LANG=en' ],
    cwd         => $redmine::home,
    unless      => "PGPASSWORD=${redmine::password} /usr/bin/psql \
                                                    -U ${redmine::user} \
                                                    -h ${redmine::db_host} \
                                                    ${redmine::db} \
                                                    -c '\\dt' \
                                                  | /usr/bin/grep custom_field_enumerations",
    require     => Exec['generate_token']
  }

  # copy web files from examples
  exec { 'configure_web':
    command => "/usr/bin/cp -p ${redmine::home}/public/dispatch.fcgi.example ${redmine::home}/public/dispatch.fcgi \
             && /usr/bin/cp -p ${redmine::home}/public/htaccess.fcgi.example ${redmine::home}/public/.htaccess",
    creates => [  "${redmine::home}/public/.htaccess",
                  "${redmine::home}/public/dispatch.fcgi" ],
    require => Exec['populate_database']
  }

  # enable apache2 modules
  apache2::utils::enable_module { 'enable_redmine_apache_modules':
    modules => ['ssl', 'headers', 'expires', 'passenger'],
    depends => Exec['configure_web']
  }

  # configure apache site
  apache2::utils::site { 'create_redmine_apache_site':
    site    => $::fqdn,
    config  => epp('redmine/site.epp'),
    depends => Apache2::Utils::Enable_module['enable_redmine_apache_modules']
  }

  # after all packages installed and all configuration are made
  # restart apache2 to be shure that all parametes are applyed correctly
  apache2::utils::restart { 'final_apache2_redmine_restart': }


}
