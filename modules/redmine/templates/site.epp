# All requests to HTTP port redirect to HTTPS
<VirtualHost *:80>
  ServerName <%= $::fqdn %>
  ServerAlias www.<%= $::fqdn %>
  Redirect permanent / https://<%= $::fqdn %>/
</VirtualHost>


# Configure HTTPS Site
<VirtualHost *:443>
  ServerName <%= $::fqdn %>
  ServerAlias www.<%= $::fqdn %>
  
  SSLEngine on
  SSLCertificateFile "/opt/cfssl/apache2/ca.pem"
  SSLCertificateKeyFile "/opt/cfssl/apache2/ca-key.pem"

  # to allow embed redmine to nextcloud iframe
  Header always unset X-Frame-Options

  RailsEnv production
  DocumentRoot <%= $redmine::home %>/public

  <Directory "<%= $redmine::home %>/public">
    Options -Indexes
    Options -MultiViews

    SetEnv SSL_CERT_DIR /etc/ssl/certs
    SetEnv SSL_CERT_FILE /etc/ssl/certs/<%= $::domain %>.pem

    <IfModule mod_authz_core.c>
      # apache 2.4+
      Require all granted
    </IfModule>

    <IfModule !mod_authz_core.c>
      Order deny,allow
      Allow from all
    </IfModule>
  </Directory>

  # Send expiry headers for assets, that carry an asset id. Assuming, an asset
  # id is a unix timestamp, which is currently a 10 digit integer. This might
  # change in the far future.
  <FilesMatch "\.(ico|pdf|flv|jpg|jpeg|png|gif|js|css|swf)$">
    ExpiresActive On
    ExpiresDefault "access plus 1 year" 
  </FilesMatch>
  
</VirtualHost>
