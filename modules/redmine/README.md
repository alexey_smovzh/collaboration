### 0. Description
This module install Redmine project management 
and issue tracker software.

Apache web server configured to accept only HTTPS connections
on 443 port. For SSL certificate management are used Cloudflare
CfSSL manager. 

As database in this installation are used PostgreSQL. 

At last steps after main Redmine installation this module can configure
LDAPS connections to some LDAP server for user management. 

Also this module can download and install custom Redmine theme.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Provide required values in Hiera yaml

```
  # CfSSL configuration
  ca:
    home: '/opt/cfssl'
    certificates:
      apache2:
        profile: 'service'
        path: '/opt/cfssl/apache2'
        owner: 'www-data'
        group: 'www-data'


  redmine:
    # folder to install Redmine into
    home: '/opt/redmine'
    # Redmine database name
    database: 'redmine'
    # url link to Redmine distribution archive file
    link: 'https://www.redmine.org/releases/redmine-4.2.1.tar.gz'
    # folder to store attachments
    # its better place not in Redmine home folder
    attachments: '/var/opt/redmine/files'
    # custom Redmine theme parameters
    theme:
      # theme name
      name: 'Bleuclair'
      # url link to theme sources archive file
      link: 'https://github.com/farend/redmine_theme_farend_bleuclair/archive/refs/tags/0.4.0.tar.gz'  

```

Install required modules and Redmine itself

```
class role::redmine {

  include postgresql::client
  include cfssl::manager
  include apache2::install
  include redmine::install
  # if don't need ldap authentification skip this step
  include redmine::ldap
  # if don't need custom theme skip this step
  include redmine::theme

  Class['postgresql::client']
  -> Class['cfssl::manager']
  -> Class['apache2::install']
  -> Class['redmine::install']
  -> Class['redmine::ldap']
  -> Class['redmine::theme']

}

```


### 3. Known backgrounds and issues
*important: update redmine configuration settings via [templates/settings.yml.epp](templates/settings.yml.epp).*
*If you edit settings from redmine Web UI it will be overwritten on next Puppet run*

If you get in a browser error code 500 and in /opt/redmine/log/production.log 
there are some incomprehensible stack trace. 
First try to replace file <home>/config/settings.yml with default one from 
redmine installation archive. The most possible reason is in some changes 
in configuration file and redmine crushed on old one.


### 4. Used documentation
https://www.redmine.org/projects/redmine/wiki/redmineinstall

https://www.atlantic.net/dedicated-server-hosting/how-to-install-redmine-4-2-on-debian-10/
