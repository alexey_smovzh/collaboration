# Create and enable apache2 site
# 
# @site         - FQDN name of site
# @config       - virtual host configuration file as plain text
# @depends      - requirements to run this function
#
define apache2::utils::site (

    String $site,
    String $config,
    Optional[Any] $depends = undef

  ) {

  # site apache config
  file { "/etc/apache2/sites-available/${site}.conf":
    ensure  => present,
    content => $config,
    notify  => Apache2::Utils::Restart["reload_apache_${site}"],
    require => $depends
  }

  # enable site
  exec { "enable_site_${site}":
    command  => "/usr/sbin/a2ensite ${site}",
    provider => 'shell',
    creates  => "/etc/apache2/sites-enabled/${site}.conf",
    notify   => Apache2::Utils::Restart["reload_apache_${site}"],
    require  => File["/etc/apache2/sites-available/${site}.conf"]
  }

  # reload configuration
  apache2::utils::restart { "reload_apache_${site}": }

}



