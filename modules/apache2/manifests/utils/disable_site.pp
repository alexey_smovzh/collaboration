# Disable apache2 site
# 
# @site           - name of site to disable
# @depends        - requirements to run this function
#
define apache2::utils::disable_site (

    String $site,
    Optional[Any] $depends = undef

  ) {


  # disable site
  exec { "disable_site_${site}":
    command  => "/usr/sbin/a2dissite ${site}",
    provider => 'shell',
    onlyif   => "/usr/sbin/apache2ctl -t -D DUMP_VHOSTS | /usr/bin/grep ${site}",
    notify   => Apache2::Utils::Restart["reload_apache_${site}_${::hostname}"],
    require  => $depends
  }

  # reload configuration
  apache2::utils::restart { "reload_apache_${site}_${::hostname}": }

}
