# Restart Apache2 web server
#
define apache2::utils::restart (

  ) {


  if !defined(Exec['restart_apache']) {

    # restart apache
    exec { 'restart_apache':
      command     => '/usr/sbin/apachectl -k restart',
      provider    => 'shell',
      refreshonly => true
    }

  }

}
