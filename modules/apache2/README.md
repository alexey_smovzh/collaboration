### 0. Description
This module install Apache2 Web Server and provides several
Puppet defined fuctions for configuration from another modules.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Install

```
  include apache2::install
```

Example of Apache2 configuration from another module

```
  # enable apache2 modules
  # module names should passed in Array
  apache2::utils::enable_module { 'enable_apache_modules':
    modules => ['proxy_http', 'mod_rewrite'],
    depends => Package[$packages]
  }

  # configure apache site
  apache2::utils::site { 'create_apache_site':
    site    => "${::hostname}.${::domain}",
    config  => template('module/site.erb'),
    depends => Package[$packages]
  }
```

Where 'module/site.erb' are the Apache2 site template. 
For example site template with reverse proxy and HTTP
authentification with htpasswd.

```
<VirtualHost <%= @hostname %>:<%= @port %>>
    ServerName <%= @hostname %>.<%= @domain %>

    ProxyRequests Off
    ProxyVia Off
    ProxyPreserveHost On

    <Proxy *>
        Require all granted
    </Proxy>

    <Location /login/>
        AuthType Basic
        AuthName "Example HTTP Authentification"
        AuthUserFile <%= @htpasswd %>
        Require valid-user
    </Location>

    AllowEncodedSlashes On
    ProxyPass / http://127.0.0.1:8080/ nocanon
</VirtualHost>
```


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation
https://httpd.apache.org/docs/2.4/
