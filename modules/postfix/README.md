### 0. Description
This module install Dovecot mail server.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage


```
  # view all available configuration parameters
  sudo postconf -d

  # check postfix configuration
  sudo postfix check

  # test domain
  postmap -q example.com ldap:/etc/postfix/virtual_domain.cf

  # check user configuration
  sudo postmap -q olya@west ldap:/etc/postfix/ldap_initial.cf

  # view mail queue
  sudo postqueue -p

  # purge email queue
  sudo postsuper -d ALL

  # check configuration parameter
  postconf transport_maps

  # check TLS connection
  sudo -u postfix openssl s_client -CAfile /etc/ssl/certs/west.pem -key /opt/cfssl/postfix/ca-key.pem -cert /opt/cfssl/postfix/ca.pem -connect dovecot-initial.west:24
  
  # send test mail (require mailutils installed)
  echo "This is the body of the email" | mail -s "This is the subject line" vasya@nextcloud.west

  echo "This is the body of the email" | mail -aFrom:misha@west -s "This is the subject line" olya@west

```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
https://repo.dovecot.org/

https://123qwe.com/tutorial-debian-10/#dovecot

https://www.linode.com/docs/guides/email-with-postfix-dovecot-and-mysql/
