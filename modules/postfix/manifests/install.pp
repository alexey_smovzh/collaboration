# Install Postfix MTA package
#
class postfix::install inherits postfix {


  # install packages
  package { $postfix::packages:
    ensure  => installed
  }


}
