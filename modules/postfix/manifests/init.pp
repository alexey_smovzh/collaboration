# Init class
#
class postfix {

  # Hiera values
  $units            = lookup('units', Tuple)

  $ca               = lookup('ca.certificates.postfix', Hash)
  $ca_path          = $ca['path']

  $ldap             = lookup('products.ldap', Hash)
  $ldap_host        = $ldap['host']
  $ldap_password    = $ldap['unit_viewer_password']

  $imap             = lookup('products.imap.name', String)
  $relay            = lookup('products.relay.host', String)
  $spam             = lookup('products.spam.host', String)

  # email attachment size limit
  $attachment_size  = lookup('attachment_size', String)

  $net_allowed      = lookup('net_allowed', String)


  # static values
  $packages   = [ 'postfix',
                  'postfix-ldap'  ]
  $services   = [ 'postfix' ]


}
