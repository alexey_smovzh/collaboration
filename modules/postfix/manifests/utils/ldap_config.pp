# Create separate LDAP configuration file for each unit
# 
# @units          - Units list
# @host           - LDAP server hostname or IP address
# @password       - user account name to run commands from
# @reload         - service to notify about changes
# @depends        - requirements to run this function
#
define postfix::utils::ldap_config (

    Tuple $units,
    String $host,
    String $password,
    Optional[Any] $reload = undef,
    Optional[Any] $depends = undef,

  ) {

  $units.each |$unit| {

    # LDAP connection to users database
    file { "/etc/postfix/ldap_${unit}.cf":
      ensure  => present,
      content => epp('postfix/ldap.cf.epp', {'unit' => $unit, 'host' => $host, 'password' => $password}),
      notify  => $reload,
      require => $depends
    }

  }


}
