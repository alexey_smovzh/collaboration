# Postfix main server in private network
#
class postfix::server inherits postfix {


  # LDAP connection to users database
  postfix::utils::ldap_config { 'configure_ldap_connections':
    units    => $postfix::units,
    host     => $postfix::ldap_host,
    password => $postfix::ldap_password,
    reload   => Service[$postfix::services]
  }

  # LDAP connection to email aliases database
  file { '/etc/postfix/ldap_alias_maps.cf':
    ensure  => present,
    content => epp('postfix/ldap_aliases.cf.epp'),
    notify  => Service[$postfix::services],
    require => Postfix::Utils::Ldap_config['configure_ldap_connections']
  }

  # configuration file
  file { '/etc/postfix/main.cf':
    ensure  => present,
    content => epp('postfix/main.cf.epp'),
    notify  => Service[$postfix::services],
    require => File['/etc/postfix/ldap_alias_maps.cf']
  }

  # master process configuration file
  file { '/etc/postfix/master.cf':
    ensure  => present,
    content => epp('postfix/master.cf.epp'),
    notify  => Service[$postfix::services],
    require => File['/etc/postfix/main.cf']
  }

  # run service
  service { $postfix::services:
    ensure  => running,
    enable  => true,
    require => File['/etc/postfix/master.cf']
  }


}
