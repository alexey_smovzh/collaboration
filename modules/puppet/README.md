### 0. Description
This module install and configure Puppet Server, Agent and PDK.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
For Puppet server in Hiera provide following configuration information

```
  # Merge deep with Puppet parameters defined 
  # on higher Hiera hierarchy levels
  lookup_options:
    puppet: 
      merge: deep

  # List of domains granted to autosign
  puppet:
    autosign:
      - west
      - east
```

Install Puppet Server

```
  include puppet::server
```

For Puppet Agent provide in Hiera next information

*note: usually this information are placed on domain level*

```
  # local domain puppet configuration
  puppet:
    name: puppet-server
    address: 10.64.30.210
    environment: production
```

Install Puppet Agent

```
  include puppet::agent
```

Install Puppet PDK

```
  include puppet::pdk
```


### 3. Known backgrounds and issues
not found


### 4. Used documentation

