# Install Puppet Agent
#
class puppet::agent inherits puppet::install {


  # install 
  package { 'puppet-agent':
    ensure  => installed
  }

  # configure puppet agent
  file { '/etc/puppetlabs/puppet/puppet.conf':
    ensure  => present,
    content => epp('puppet/puppet.conf.epp'),
    notify  => Service['puppet'],
    require => Package['puppet-agent']
  }

  # ensure service is disabled and not running
  service { 'puppet':
    ensure  => stopped,
    enable  => false,
    require => Package['puppet-agent']
  }

}
