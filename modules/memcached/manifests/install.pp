# Install memcached
#
class memcached::install inherits memcached {

  # install 
  package { $memcached::packages:
    ensure => installed
  }

  # create config
  file { '/etc/memcached.conf':
    ensure  => present,
    content => "-l ${memcached::bind} -u memcache",
    require => Package[$memcached::packages],
    notify  => Service[$memcached::services]
  }

  # ensure memcached service running and enabled
  service { $memcached::services:
    ensure   => running,
    provider => 'systemd',
    enable   => true,
    require  => File['/etc/memcached.conf']
  }

}
