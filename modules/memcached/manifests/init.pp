# Init class
#
class memcached {

  $packages = ['memcached']
  $services = ['memcached']

  # parameters from Hiera
  $memcached = lookup('memcached', Hash)

  $bind = $memcached['bind']

}

