# Init class
#
class postgresql {


  # hiera values
  $network        = lookup('net_allowed', String)

  $database       = lookup('products.database', Hash)
  $administrator  = $database['administrator']
  $password       = $database['password']


  $version    = '13'
  $repository = '/etc/apt/sources.list.d/postgresql.list'
  $key        = 'https://www.postgresql.org/media/keys/ACCC4CF8.asc'
  $value      = "deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt/ ${::lsbdistcodename}-pgdg main"

  $services   = ['postgresql']


}

