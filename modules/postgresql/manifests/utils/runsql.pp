# Execute SQL query
#
# @db             - database name
# @administrator  - login name of database administrator
# @password       - database administrator password
# @query          - SQL query to execute
# @check          - SQL request to check if @query need to be executed
# @depends        - requirements to run this function
#
define postgresql::utils::runsql (

    String $db,
    String $administrator,
    String $password,
    String $query,
    Optional[String] $check = '',
    Optional[Any] $depends = undef,
    Optional[String] $host = 'localhost'

) {


  exec { "run_sql_query_on_${db}":
    command  => "PGPASSWORD=${password} /usr/bin/psql -U ${administrator} \
                                                        -h ${host} \
                                                        ${db} \
                                                        -c \"${query}\"",
    provider => 'shell',
    require  => $depends,
    unless   => "PGPASSWORD=${password} /usr/bin/psql -U ${administrator} \
                                        -h ${host} \
                                        ${db} \
                                        -c \"${check}\" \
                                      | /usr/bin/grep -w '1 row'"
  }

}

