# Create database
#
# @db             - database name to create
# @administrator  - login name of database administrator
# @password       - database administrator password
# @depends        - requirements to run this function
#
define postgresql::utils::database (

    String $db,
    String $administrator,
    String $password,
    Any $depends,
    Optional[String] $host = 'localhost'

) {


  exec { "create_database_${db}":
    command  => "PGPASSWORD=${password} /usr/bin/psql -U ${administrator} \
                                                      -h ${host} \
                                                      postgres \
                                                      -c 'CREATE DATABASE ${db};'",
    provider => 'shell',
    require  => $depends,
    unless   => "PGPASSWORD=${password} /usr/bin/psql -l -U ${administrator} -h ${host} \
                                      | /usr/bin/grep ${db}"
  }

}

