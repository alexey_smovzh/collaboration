# Create Database user and grant privileges on database
#
# @db               - database name to create
# @administrator    - login name of database administrator
# @password         - database administrator password
# @user             - login name of new user
# @user_password    - password of new user
# @depends          - requirements to run this function
# @host             - database server hostname
#
define postgresql::utils::user (

    String $db,
    String $administrator,
    String $password,
    String $user,
    String $user_password,
    Any $depends,
    Optional[String] $host = 'localhost'

) {


  exec { "create_user_${user}_for_${db}":
    command  => "PGPASSWORD=${password} /usr/bin/psql \
                                        -U ${administrator} \
                                        -h ${host} \
                                        ${db} \
                                        -c \"CREATE USER ${user} WITH ENCRYPTED PASSWORD '${user_password}'; \
                                        GRANT ALL PRIVILEGES ON DATABASE ${db} to ${user}; \"",
    provider => 'shell',
    require  => $depends,
    unless   => "PGPASSWORD=${password} /usr/bin/psql \
                                        -U ${administrator} \
                                        -h ${host} \
                                        ${db} \
                                        -c \"SELECT 1 FROM pg_roles WHERE rolname='${user}'\" | /usr/bin/grep '1 row'"
  }

}
