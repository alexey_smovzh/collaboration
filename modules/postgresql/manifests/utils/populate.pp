# Populate database schema
#
# @db             - database name
# @administrator  - login name of database administrator
# @password       - database administrator password
# @schema         - SQL schema of the database
# @depends        - requirements to run this function
#
define postgresql::utils::populate (

    String $db,
    String $administrator,
    String $password,
    String $schema,
    Any $depends,
    Optional[String] $host = 'localhost'

) {


  exec { "populate_database_${db}":
    command  => "PGPASSWORD=${password} /usr/bin/psql -U ${administrator} \
                                                        -h ${host} \
                                                        ${db} \
                                                        -c \"${schema}\"",
    provider => 'shell',
    require  => $depends,
    onlyif   => "PGPASSWORD=${password} /usr/bin/psql -t -U ${administrator} \
                                        -h ${host} \
                                        ${db} \
                                        -c \"select count(*) from information_schema.tables where table_schema='public';\" \
                                      | /usr/bin/grep -w 0"
  }

}

