# Install PostgreSQL Server and Client
#
class postgresql::install inherits postgresql {


  include postgresql::server
  include postgresql::client


}
