# Install PostgreSQL Server
#
class postgresql::server inherits postgresql {


  if !defined(Apt::Utils::Apt_repository['postgresql']) {

    # install posgresql apt repository
    apt::utils::apt_repository { 'postgresql':
      key        => $postgresql::key,
      file       => $postgresql::repository,
      repository => $postgresql::value,
    }

  }

  # install server package
  package { "postgresql-${postgresql::version}":
    ensure  => installed,
    require => Apt::Utils::Apt_repository['postgresql']
  }

  # create server configuration
  file {  "/etc/postgresql/${postgresql::version}/main/postgresql.conf":
    ensure  => present,
    owner   => 'postgres',
    group   => 'postgres',
    content => epp('postgresql/postgresql.conf.epp'),
    notify  => Service[$postgresql::services],
    require => Package["postgresql-${postgresql::version}"]
  }

  # update pg_hba connection configuration
  file {  "/etc/postgresql/${postgresql::version}/main/pg_hba.conf":
    ensure  => present,
    owner   => 'postgres',
    group   => 'postgres',
    content => epp('postgresql/pg_hba.conf.epp'),
    notify  => Service[$postgresql::services],
    require => File["/etc/postgresql/${postgresql::version}/main/postgresql.conf"]
  }

  # ensure service is running and enabled
  service { $postgresql::services:
    ensure  => running,
    enable  => true,
    require => File["/etc/postgresql/${postgresql::version}/main/pg_hba.conf"]
  }

  # create administrator
  exec { 'create_database_administrator':
    command  => "/usr/bin/psql -c \"CREATE ROLE ${postgresql::administrator} \
                                    WITH LOGIN SUPERUSER \
                                    ENCRYPTED PASSWORD '${postgresql::password}'\"",
    provider => 'shell',
    user     => 'postgres',
    group    => 'postgres',
    unless   => "PGPASSWORD=${postgresql::password} /usr/bin/psql 
                                                    -U ${postgresql::administrator} \
                                                    -h ${postgresql::host} \
                                                    postgres \
                                                    -c \"SELECT 1 FROM pg_roles WHERE rolname='${postgresql::administrator}'\" \
                                                  | /usr/bin/grep '1 row'",
    require  => Service[$postgresql::services]
  }

}
