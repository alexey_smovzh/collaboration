# Install PostgreSQL Client
#
class postgresql::client inherits postgresql {


  if !defined(Apt::Utils::Apt_repository['postgresql']) {

    # install posgresql apt repository
    apt::utils::apt_repository { 'postgresql':
      key        => $postgresql::key,
      file       => $postgresql::repository,
      repository => $postgresql::value,
    }

  }

  # install client package
  package { "postgresql-client-${postgresql::version}":
    ensure  => installed,
    require => Apt::Utils::Apt_repository['postgresql']
  }


}
