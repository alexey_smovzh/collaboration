### 0. Description
This module install PostgreSQL in single server mode.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Provide PostgreSQL initial parameters in Hiera yaml file

```
    postgresql:
      version: 13

# todo: update readme to final configuration

    # services database parameters
    databases: 
        giteadb:
            config: 99-giteadb
            dbuser: gitea
            dbpass: gitea
            # all values below are optional 
            characterset: utf8mb4 
            collation: utf8mb4_unicode_ci
```

Then include install class 

```
    # Install
    include postgresql::install

```

**OR with profiles**

If several instances have the different roles but must share 
the same Hiera yaml configuration. This can be archived with 
profiles. With profiles nodes can have different 'roles', 
but the same 'profile'.

Create file in Hiera node hierarhy with FQDN name of node.
Define node profile or profiles in Array. In result for 
this node will be available all parameters from Hiera profiles.
For example for 'profile/cicd.yaml' and 'profile/runners.yaml'.

``` 
    profiles:
      - 'cicd'
      - 'runners'
``` 

Install PostgreSQL as usual

``` 
    include postgresql::install
``` 


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation
