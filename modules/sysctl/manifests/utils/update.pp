# Update sysctl OS parameters
#
# Change values in /etc/sysctl.conf for persistent configuration
# and update current running values to change make effect without rebooting
# 
# @options        - Hash with options to change
#                   where 'key' is a option name
#                   and 'value' is value
#
define sysctl::utils::update (

    Hash $options

  ) {


  $options.each |$key, $value| {

    # set parameter with augeas tool
    augeas { "add_sysctl_conf_${key}":
      context => '/files/etc/sysctl.conf',
      changes => [ "set ${key} ${value}" ],
      onlyif  => "get ${key} != ${value}"
    }

    # update running values
    exec { "update_runtime_${key}":
      command  => "/usr/sbin/sysctl ${key}=${value}",
      provider => 'shell',
      user     => 'root',
      group    => 'root',
      unless   => "/usr/sbin/sysctl --values ${key} | /usr/bin/grep ${value}"
    }

  }

}
