### 0. Description
This module provide functions to manage Sysctl OS parameters.

*note: because supposed that this module will be used first in*
*any manifest. There are no parameres 'require' or 'notify'.*
*If you need them, you should add.*


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Pass sysctl parameters names and its values in Hash

```
  sysctl::utils::update { 'set_OS_parameters':
    options => { 'vm.overcommit_memory' => '1', 
                 'net.core.somaxconn' => '1024'  }
  }
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
