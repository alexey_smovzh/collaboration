### 0. Description
This module install Redis in-memory data store.

Please note:
In this module Redis Server binds only to localhost 127.0.0.1
via IPv4. Network connections are not included.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Before installation update Redis configuration to production
resonable values [hiera/role/rspamd.yaml](hiera/role/rspamd.yaml).

```
  redis:
    # memory limit in megabytes
    memory: '128'  
```

Then install
```
  include redis::install
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
https://rspamd.com/doc/quickstart.html
