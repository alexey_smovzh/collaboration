# Install Redis
#
class redis::install inherits redis {


  # adjust OS parameters for Redis
  sysctl::utils::update { 'set_OS_parameters':
    options => {  'vm.overcommit_memory' => '1',
                  'net.core.somaxconn'   => '1024'  }
  }

  # install packages
  package { $redis::packages:
    ensure  => installed,
    require => Sysctl::Utils::Update['set_OS_parameters']
  }

  # configuration file
  file { '/etc/redis/redis.conf':
    ensure  => present,
    content => epp('redis/redis.conf.epp'),
    notify  => Service[$redis::services],
    require => Package[$redis::packages]
  }

  # run service
  service { $redis::services:
    ensure  => running,
    enable  => true,
    require => File['/etc/redis/redis.conf']
  }


}

# 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' /etc/rc.local 
