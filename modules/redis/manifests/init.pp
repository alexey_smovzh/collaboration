# Init class
#
class redis {


  # Hiera values
  $redis      = lookup('redis', Hash)
  $memory     = $redis['memory']


  # static values
  $packages   = [ 'redis-server' ]
  $services   = [ 'redis' ]


}
