### 0. Description
This module install Rspamd - spam filtering system.

*note: without web interface*


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Configure RAM limit for Redis in Hiera yaml file

```
  redis:
    # memory limit in megabytes
    memory: '128'
```

Install server
```
  include rspamd::install
```

Install rspamc on another host to report spam/ham to rspamd server
```
  include rspamd::client
```

Usefull commands
```
  # Spam learning statistics
  rspamc -v -h rspamd.west:11334 -P q1 stat

  # Send message to learn (learn_spam/learn_ham)
  rspamc -v -h rspamd.west:11334 -P q1 learn_spam one.eml 
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
https://rspamd.com/doc/quickstart.html

https://thomas-leister.de/en/mailserver-debian-stretch/
