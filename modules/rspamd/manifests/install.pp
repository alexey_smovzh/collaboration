# Install Rspamd
#
class rspamd::install inherits rspamd {


  # install rspamd apt repository
  apt::utils::apt_repository { 'rspamd':
    key        => $rspamd::key,
    file       => $rspamd::repository,
    repository => $rspamd::value,
  }

  # install packages
  package { $rspamd::packages:
    ensure  => installed,
    require => Apt::Utils::Apt_repository['rspamd']
  }

  # create configuration files
  $rspamd::configs.each |$config| {

    # create config
    file { "/etc/rspamd/local.d/${config}":
      ensure  => present,
      content => epp("rspamd/${config}.epp"),
      notify  => Service[$rspamd::services],
      require => Package[$rspamd::packages]
    }

  }

  # todo: DKIM!


  # run service
  service { $rspamd::services:
    ensure  => running,
    enable  => true,
    require => Package[$rspamd::packages]
  }

}
