# Init class
#
class rspamd {

  # Hiera values
  $net_allowed = lookup('net_allowed', String)

  $imap        = lookup('products.imap.name', String)


  # static values
  $repository  = '/etc/apt/sources.list.d/rspamd.list'
  $key         = 'https://rspamd.com/apt-stable/gpg.key'
  $value       = "deb [arch=amd64] http://rspamd.com/apt-stable/ ${::lsbdistcodename} main"


  $packages    = [ 'rspamd' ]
  $services    = [ 'rspamd' ]


  # Configuration files  
  $configs  = [ 'classifier-bayes.conf',
                'logging.inc',
                'milter_headers.inc',
                'options.inc',
                'worker-controller.inc',
                'worker-normal.inc',
                'worker-proxy.inc'  ]


}
