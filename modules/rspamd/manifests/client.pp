# Install rspamc tool for spam reporting from another host
#
class rspamd::client inherits rspamd {


  # install rspamd apt repository
  apt::utils::apt_repository { 'rspamd':
    key        => $rspamd::key,
    file       => $rspamd::repository,
    repository => $rspamd::value,
  }

  # install packages
  package { $rspamd::packages:
    ensure  => installed,
    require => Apt::Utils::Apt_repository['rspamd']
  }

  # if we use Dovecot in this setup
  if $rspamd::imap == 'dovecot' {

    # add symlink to Dovecot 'sieve_pipe_bin_dir'
    # for Dovecot sieve be able to run 'rspamc' command
    file { '/usr/lib/dovecot/rspamc':
      ensure  => 'link',
      target  => '/usr/bin/rspamc',
      require => Package[$rspamd::packages]
    }

  }

  # stop and disable service
  service { $rspamd::services:
    ensure  => stopped,
    enable  => false,
    require => Package[ $rspamd::packages ]
  }


}
