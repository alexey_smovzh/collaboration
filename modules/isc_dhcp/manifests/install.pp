# Install ISC DHCP Server with DDNS support
#
class isc_dhcp::install inherits isc_dhcp {

  # install 
  package { $isc_dhcp::packages:
    ensure => installed,
  }

  # create config
  file { '/etc/dhcp/dhcpd.conf':
    ensure  => present,
    content => template('isc_dhcp/dhcpd.conf.erb'),
    notify  => Service[$isc_dhcp::services],
    require => Package[$isc_dhcp::packages]
  }

  # assign interface
  file { '/etc/default/isc-dhcp-server':
    ensure  => present,
    content => template('isc_dhcp/dhcpd.default.erb'),
    notify  => Service[$isc_dhcp::services],
    require => Package[$isc_dhcp::packages]
  }

  # create ddns key
  if $isc_dhcp::dns['ddns'] {
    file { '/etc/dhcp/ddns.key':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0640',
      content => template('isc_dhcp/ddns.key.erb'),
      notify  => Service[$isc_dhcp::services],
      require => Package[$isc_dhcp::packages]
    }
  }

  # ensure service is running and enabled
  service { $isc_dhcp::services:
    ensure  => running,
    enable  => true,
    require => [  File['/etc/dhcp/dhcpd.conf'],
                  File['/etc/default/isc-dhcp-server']  ]
  }

}
