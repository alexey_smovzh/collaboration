# Hardware Requirements

```
                              vCPU     RAM(Gb)       Storage(Gb)

Gitea                         2        4             100(without large files storage)
MariaDB                       2        8             20
Drone.io                      2        4             20
Puppet                        4        8             100

DNS primary                   1        4             10
DNS secondary                 1        4             10
root-ca                       1        2             10

OpenProject                   4        8             100
PostgreSQL                    4        8             40

OnlyOffice                    4        8             200
PostgreSQL                    2        8             40

Camunda

Postfix                       4        8             100

NextCloud balancer            2        8             40

Unit
NextCloud                     4        16            1024
PostgreSQL                    2        4             100
Dovecot                       2        4             200
Postfix                       1        4             10

Prometheus                    4        8             100
Graphana                      2        8             40
ElasticSearch                 4        16            500
```




# Summary for AWS Instance calculator

```
vCPU  RAM   Storage   Count

1     2     10        = 1
1     4     10        = 3
2     4     100       = 2
2     4     20        = 1
2     4     200       = 1
2     8     20        = 1
2     8     40        = 3
4     8     40        = 1
4     8     100       = 3
4     8     200       = 1
4     16    500       = 1
4     16    1024      = 1

First 12 months total         Total monthly
15,775.68 USD                 1,314.64 USD
```



# Hetzner Cloud

```
1     2     10        = 1     2.96
1     4     10        = 3     5.83
2     4     100       = 2     5.83 + 4.76
2     4     20        = 1     5.83
2     4     200       = 1     5.83 + 23.80
2     8     20        = 1     10.59
2     8     40        = 3     10.59
4     8     40        = 1     14.76
4     8     100       = 3     14.76
4     8     200       = 1     14.76 + 4.76
4     16    500       = 1     27.25 + 23.80
4     16    1024      = 1     27.25 + 48.74


First 12 months total         Total monthly
3900 EUR                      325.05 EUR

Backup 20% from instance price 
780 EUR                       65 EUR
```
