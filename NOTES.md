

- name: validate_code
  commands:
  - cd /etc/puppetlabs/code/environments/${DRONE_BRANCH}
  - /opt/puppetlabs/pdk/bin/pdk validate      



$ cat start.sh 
#!/usr/bin/env bash
#

VIRSH=/usr/bin/virsh


function instance {
    $VIRSH start $1
}

function all {
    for instance in $($VIRSH list --name --all)
    do
        instance "$instance"
    done
}


while test $# -gt 0
do
   case "$1" in
       --all) all
           ;;
       *) instance "$1"
           ;;
   esac
   shift
done



$ cat delete_instance.sh 
#!/usr/bin/env bash
#

if [ "$#" -ne 1 ]; then
    echo "Usage: `basename $0` <instance_name>"
    exit 0
else
    /usr/bin/virsh destroy $1 
    /usr/bin/virsh undefine $1 
    /usr/bin/rm -f /instances/xml/$1.xml 
    /usr/bin/rm -rf /instances/local/$1
fi

