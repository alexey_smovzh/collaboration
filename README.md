### 0. Description
Sample Office Collaboration solution based on NextCloud, 
Collabora Online, Redmine, Camunda and other OpenSource software.

![Software Diagram](collaboration.svg)


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
For installation procedure look at [INSTALL.md](INSTALL.md)


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
Look at 'used documentation' section of particular module README file.