#!/bin/bash

USER=alex
PASSWD=alex
CODE=/home/alexeysmovzh/collaboration
NODE1=10.64.30.200

# upload code to baremetal server
rsync -r -a -v -e ssh --timeout=20 --exclude '.*' --rsync-path='sudo rsync' --delete $CODE/* $USER@$NODE1:/etc/puppetlabs/code/environments/production &
# upload to puppet-server instance
rsync -r -a -v -e ssh --timeout=20 --exclude '.*' --rsync-path='sudo rsync' --chown=puppet:puppet --delete $CODE/* $USER@10.64.30.210:/etc/puppetlabs/code/environments/production &
wait

# deploy services
ssh $USER@$NODE1 "echo $PASSWD | sudo -S /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/" &
wait

