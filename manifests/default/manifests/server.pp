# Default services for all nodes and instances
#
class default::server {

  include apt
  include network
  include ntp
  include ssh
  include administrators

  # Service Agents 
  include puppet::agent

}
