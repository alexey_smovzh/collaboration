# Deploy Mail Transfer Agent
#
class role::postfix {


  include postfix::install
  include cfssl::manager
  include postfix::server


  # we need user postfix first
  Class['postfix::install']
  -> Class['cfssl::manager']
  -> Class['postfix::server']


}
