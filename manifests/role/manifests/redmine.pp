# Deploy Redmine
#
class role::redmine {


  include postgresql::client
  include cfssl::manager
  include apache2::install
  include redmine::install
  include redmine::ldap
  include redmine::theme


  Class['postgresql::client']
  -> Class['cfssl::manager']
  -> Class['apache2::install']
  -> Class['redmine::install']
  -> Class['redmine::ldap']
  -> Class['redmine::theme']


}
