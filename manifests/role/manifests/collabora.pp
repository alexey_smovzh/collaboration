# Deploy Collabora Online
#
class role::collabora {


  include cfssl::manager
  include apache2::install
  include collabora::install


  Class['cfssl::manager']
  -> Class['apache2::install']
  -> Class['collabora::install']


}
