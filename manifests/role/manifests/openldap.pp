# Deploy OpenLDAP 
#
class role::openldap {


  include openldap::install
  include cfssl::manager
  include openldap::configuration
  include openldap::tree

  # cfssl manager require 'openldap' user and group 
  # so first install openldap than cfssl manager
  # 
  # OpenLDAP SSL configuration require ssl certificates 
  # so install cfssl manager, create certificates and than configure LDAP
  #
  # Finally upload LDAP configuration
  # and upload Organization Tree
  #
  Class['openldap::install']
  -> Class['cfssl::manager']
  -> Class['openldap::configuration']
  -> Class['openldap::tree']


}
