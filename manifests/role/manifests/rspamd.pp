# Deploy Spam Filtering System
#
class role::rspamd {


  include redis::install
  include rspamd::install


  Class['redis::install']
  -> Class['rspamd::install']


}
