# Deploy Dovecot
#
class role::dovecot {


  include rspamd::client
  include dovecot::install
  include cfssl::manager
  include dovecot::configure


  # CFSSL need that user dovecot already exists before installation
  # so firstly install Dovecot packages wich are create
  # user 'dovecot' as well. Then install CFSSL. And at last 
  # create Dovecot configureation and run service
  Class['rspamd::client']
  -> Class['dovecot::install']
  -> Class['cfssl::manager']
  -> Class['dovecot::configure']


}
