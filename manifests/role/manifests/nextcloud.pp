# Deploy NextCloud 
#
class role::nextcloud {


  include postgresql::client
  include cfssl::manager
  include openldap::client
  include apache2::install
  include memcached::install
  include nextcloud::install
  include nextcloud::postinstall

  Class['postgresql::client']
  -> Class['cfssl::manager']
  -> Class['openldap::client']
  -> Class['apache2::install']
  -> Class['memcached::install']
  -> Class['nextcloud::install']
  -> Class['nextcloud::postinstall']


}
