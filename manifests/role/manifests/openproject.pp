# Deploy OpenProject 
#
class role::openproject {


  include postgresql::client
  include cfssl::manager
  include openldap::client
  include apache2::install
  include memcached::install
  include openproject::install

  Class['postgresql::client']
  -> Class['cfssl::manager']
  -> Class['openldap::client']
  -> Class['apache2::install']
  -> Class['memcached::install']
  -> Class['openproject::install']


}
