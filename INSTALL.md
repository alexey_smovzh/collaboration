
### 0. Install OS on baremetal server

Download and install latest Debian Linux from minimal 
netinstall image

https://www.debian.org/distrib/netinst


### 1. Copy your public SSH key 

```    
    ssh-copy-id <user>@<host>
```


### 2. Disable Spectre and Meltdown patches

*This step is optional If you don't wont disable security pathes you can skip it*

```
    su -
    vi /etc/default/grub

    GRUB_CMDLINE_LINUX_DEFAULT="splash quiet pti=off kpti=off spectre_v2=off spec_store_bypass_disable=off"

    update-grub
    init 6
```


### 3. Install depencies and Puppet agent

```
    su -
    apt install sudo wget rsync
    exit
    wget https://apt.puppetlabs.com/puppet6-release-buster.deb
    sudo dpkg -i puppet6-release-buster.deb 
    sudo apt update
    sudo apt install puppet-agent
```  


### 4. Check and disable unnecessarily services

*This step is optional. You can skip it*

```
    systemctl list-units *.service

    sudo systemctl disable ufw
    sudo systemctl disable apparmor
    sudo systemctl disable puppet
    sudo systemctl disable anacron
    sudo systemctl disable apt-daily.timer 
    sudo systemctl disable apt-daily-upgrade.timer
```


### 5. Edit setup configuration in Hiera
Edit parameters in theres files with your network and environment values.
Parameters names and values are self explanatory.

- [hiera/role/kvm.yaml](hiera/role/kvm.yaml)
- [hiera/global/network.yaml](hiera/global/network.yaml)
- [hiera/global/administrators.yaml](hiera/global/administrators.yaml)


### 6. Edit parameters in deploy.sh and setup services
[deploy.sh](deploy.sh) script

```
    vi <repository_path>/deploy.sh

    USER=alex
    PASSWD=alex
    CODE=/home/alexeysmovzh/collaboration
    NODE1=10.64.30.200

    # run deploynment script
    <repository_path>/deploy.sh
```


### 7. Reboot node
When Puppet successfully finish its job restart baremetal server
to enshure that all configuration parameters in all applications 
and on operating system are definitely applied.


### 8. Start instances
[deploy.sh](deploy.sh) only creates instances and does not run it automatically.

*note: you can change this behavior if add code to start instance automatically*
*after it had been created into* 
*[modules/kvm/manifests/utils/instance.pp](modules/kvm/manifests/utils/instance.pp) manifest* 

After deploy.sh successfully complete its job you should start instances by hand
in following order

```
    # list all instances
    sudo virsh list --all

    # first start and wait until it will be ready puppet server
    # other instances are install their software by puppet agents 
    # with configuration obtained from puppet server
    sudo virsh start puppet
    sudo virsh console puppet

    # then start and wait until it will be ready dns server and cfssl (root-ca)
    # because other instances require this services to complete its own installation succesfully
    sudo virsh start dns
    sudo virsh start root-ca

    # then start other instances in any order
    sudo virsh start openldap
    sudo virsh start postgresql
    ...

```
